//
//  QuestionViewController.swift
//  Payke
//
//  Created by ShinjiYamamoto on 2019/05/30.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//

import UIKit
import AVFoundation

class QuestionViewController: UIViewController {

    
    var audioPlayerInstance : AVAudioPlayer! = nil  // 再生するサウンドのインスタンス
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemMainLabel: UILabel!
    @IBOutlet weak var itemSubLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    

    
    var item: ItemModel? {
        didSet {
            Api.Img.configureCell(with: item!.imgUrl[0], imageView: self.itemImageView)
            
            self.itemMainLabel.text! = self.item!.title
            self.itemSubLabel.text! = self.item!.subTitle
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        backView.layer.shadowColor = UIColor.black.cgColor
        backView.layer.shadowOpacity = 0.2
        backView.layer.shadowOffset = .init(width: 0, height: 4)
        backView.layer.shadowRadius = 6
        
        
        // サウンドファイルのパスを生成
        let soundFilePath = Bundle.main.path(forResource: "Kid_Laugh", ofType: "mp3")!
        let sound:URL = URL(fileURLWithPath: soundFilePath)
        // AVAudioPlayerのインスタンスを作成
        do {
            audioPlayerInstance = try AVAudioPlayer(contentsOf: sound, fileTypeHint:nil)
        } catch {
            print("AVAudioPlayerインスタンス作成失敗")
        }
        // バッファに保持していつでも再生できるようにする
        audioPlayerInstance.prepareToPlay()
        // Do any additional setup after loading the view.
        
        
    }

    @IBAction func playSounds(_ sender: Any) {
        audioPlayerInstance.play()
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
