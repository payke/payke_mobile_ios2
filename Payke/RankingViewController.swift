//
//  RankingViewController.swift
//  Payke
//
//  Created by ShinjiYamamoto on 2019/04/14.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//

import UIKit
import FittedSheets

class RankingViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    
    var itemsArray:[RankingModel] = []
    
    override func viewDidAppear(_ animated: Bool) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.tableView.reloadData()
        }
    }
    
    
    private var selectedImageView: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 60
        tableView.rowHeight = UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RankingTableViewCell", for: indexPath) as! RankingTableViewCell
            let item = itemsArray[indexPath.row]
            cell.item = item
            cell.delegate = self
            cell.noView.layer.borderColor = UIColor.yellow.cgColor
            cell.noView.layer.borderWidth = 3
            cell.noLabel.text! = "\(indexPath.row + 1)"
            
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RankingTableViewCell", for: indexPath) as! RankingTableViewCell
            let item = itemsArray[indexPath.row]
            cell.item = item
            cell.delegate = self
            cell.noView.layer.borderColor = UIColor.gray.cgColor
            cell.noView.layer.borderWidth = 3
            cell.noLabel.text! = "\(indexPath.row + 1)"
            
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RankingTableViewCell", for: indexPath) as! RankingTableViewCell
            let item = itemsArray[indexPath.row]
            cell.item = item
            cell.delegate = self
            cell.noView.layer.borderColor = UIColor.brown.cgColor
            cell.noView.layer.borderWidth = 3
            cell.noLabel.text! = "\(indexPath.row + 1)"
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RankingTableViewCell", for: indexPath) as! RankingTableViewCell
            let item = itemsArray[indexPath.row]
            cell.item = item
            cell.delegate = self
            cell.noView.layer.borderColor = UIColor.gray.cgColor
            cell.noView.layer.borderWidth = 1
            cell.noLabel.text! = "\(indexPath.row + 1)"
            
            
            return cell
        }
    }
    
}
extension RankingViewController: RankingTableViewCellDelegate {
    
    
    func goToZoom(itemview: UIImageView, no: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.setData = (no,1)
        
        //performSegue(withIdentifier: "segue", sender: nil)
        let controller = SheetViewController(controller: UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "PSHTMLInTableViewController"), sizes: [.fullScreen])
        
        self.present(controller, animated: false, completion: nil)
        
    }
    
    
}
