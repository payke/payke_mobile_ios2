//
//  RatingViewController.swift
//  PopupDialog
//
//  Created by Martin Wildfeuer on 11.07.16.
//  Copyright © 2016 CocoaPods. All rights reserved.
//

import UIKit


class RatingViewController: UIViewController {


    @IBOutlet weak var textView: UITextView!

    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        
        textView.delegate = self
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(endEditing)))
        
        self.textView.layer.borderColor = UIColor.darkGray.cgColor
        self.textView.layer.borderWidth = 0.5
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.textView.becomeFirstResponder()
        }
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @objc func endEditing() {
        view.endEditing(true)
    }
}

extension RatingViewController: UITextViewDelegate {


}
