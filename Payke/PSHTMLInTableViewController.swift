//
//  HTMLInTableViewController.swift
//  PSHTMLView
//
//  Created by Predrag Samardzic on 23/11/2017.
//  Copyright © 2017 Predrag Samardzic. All rights reserved.
//

import UIKit
import SafariServices
import WebKit
import MessageUI
import PSHTMLView
import PopupDialog

class PSHTMLCell: UITableViewCell {
    @IBOutlet weak var htmlView : PSHTMLView!
}

class PSHTMLInTableViewController: UIViewController {
    @IBOutlet weak var buttomButton: UIButton!
    @IBOutlet weak var  progressView: UIProgressView!
    @IBOutlet weak var tableView: UITableView!
    var img:UIImage?
    
    
    var item:ItemModel?
    var commentItem:[CommentModel] = []
    var bottomSafeAreaHeight: CGFloat = 0
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    lazy private var htmlCell: PSHTMLCell = { [weak self] in
        var cell = self?.tableView.dequeueReusableCell(withIdentifier: "HTMLCell") as! PSHTMLCell
        cell.htmlView.delegate = self
        return cell
        }()
    
    
    
    var draggingDownToDismiss = false
    var interactiveStartingPoint: CGPoint?
    var dismissalAnimator: UIViewPropertyAnimator?
    final class DismissalPanGesture: UIPanGestureRecognizer {}
    final class DismissalScreenEdgePanGesture: UIScreenEdgePanGestureRecognizer {}
    
    private lazy var dismissalPanGesture: DismissalPanGesture = {
        let pan = DismissalPanGesture()
        pan.maximumNumberOfTouches = 1
        return pan
    }()
    
    private lazy var dismissalScreenEdgePanGesture: DismissalScreenEdgePanGesture = {
        let pan = DismissalScreenEdgePanGesture()
        pan.edges = .left
        return pan
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.sheetViewController?.handleScrollView(self.tableView)
        
    }
    
    func didSuccessfullyDragDownToDismiss() {
        let indexPath = IndexPath(row: 0, section: 0)
        self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
        self.dismiss(animated: true)
    }
    
    
    @IBAction func tappedDialog(_ sender: Any) {
        
        // Create a custom view controller
        let ratingVC = QuestionViewController(nibName: "QuestionViewController", bundle: nil)
        
        // Create the dialog
        let popup = PopupDialog(viewController: ratingVC,
                                buttonAlignment: .vertical,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: true,
                                panGestureDismissal: true)
        
        ratingVC.item = self.item!
        
        // Create first button
        let buttonOne = CancelButton(title: "🙅‍♂️ 売ってない", height: 60) {
            
            let alert = UIAlertController(title: "Hello", message: "How are you?", preferredStyle: UIAlertController.Style.alert)
            // add OK button
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        // Create second button
        let buttonTwo = DefaultButton(title: "🙆‍♂️ 案内する", height: 60) {
            let alert = UIAlertController(title: "Hey", message: "Are you ok?", preferredStyle: UIAlertController.Style.alert)
            // add OK button
            alert.addAction(UIAlertAction(title: "yeah", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
        // Add buttons to dialog
        popup.addButtons([buttonTwo,buttonOne])
        
        // Present dialog
        present(popup, animated: true, completion: nil)
     
    }
    
    
    // This handles both screen edge and dragdown pan. As screen edge pan is a subclass of pan gesture, this input param works.
    @objc func handleDismissalPan(gesture: UIPanGestureRecognizer) {
        
        let isScreenEdgePan = gesture.isKind(of: DismissalScreenEdgePanGesture.self)
        let canStartDragDownToDismissPan = !isScreenEdgePan && !draggingDownToDismiss
        
        // Don't do anything when it's not in the drag down mode
        if canStartDragDownToDismissPan { return }
        
        let targetAnimatedView = gesture.view!
        let startingPoint: CGPoint
        
        if let p = interactiveStartingPoint {
            startingPoint = p
        } else {
            // Initial location
            startingPoint = gesture.location(in: nil)
            interactiveStartingPoint = startingPoint
        }
        
        let currentLocation = gesture.location(in: nil)
        let progress = isScreenEdgePan ? (gesture.translation(in: targetAnimatedView).x / 100) : (currentLocation.y - startingPoint.y) / 100
        let targetShrinkScale: CGFloat = 0.86
        let targetCornerRadius: CGFloat = GlobalConstants.cardCornerRadius
        
        func createInteractiveDismissalAnimatorIfNeeded() -> UIViewPropertyAnimator {
            if let animator = dismissalAnimator {
                return animator
            } else {
                let animator = UIViewPropertyAnimator(duration: 0, curve: .linear, animations: {
                    targetAnimatedView.transform = .init(scaleX: targetShrinkScale, y: targetShrinkScale)
                    targetAnimatedView.layer.cornerRadius = targetCornerRadius
                })
                animator.isReversed = false
                animator.pauseAnimation()
                animator.fractionComplete = progress
                return animator
            }
        }
        
        switch gesture.state {
        case .began:
            dismissalAnimator = createInteractiveDismissalAnimatorIfNeeded()
            
        case .changed:
            dismissalAnimator = createInteractiveDismissalAnimatorIfNeeded()
            
            let actualProgress = progress
            let isDismissalSuccess = actualProgress >= 1.0
            
            dismissalAnimator!.fractionComplete = actualProgress
            
            if isDismissalSuccess {
                dismissalAnimator!.stopAnimation(false)
                dismissalAnimator!.addCompletion { (pos) in
                    switch pos {
                    case .end:
                        self.didSuccessfullyDragDownToDismiss()
                    default:
                        fatalError("Must finish dismissal at end!")
                    }
                }
                dismissalAnimator!.finishAnimation(at: .end)
            }
            
        case .ended, .cancelled:
            if dismissalAnimator == nil {
                // Gesture's too quick that it doesn't have dismissalAnimator!
                print("Too quick there's no animator!")
                didCancelDismissalTransition()
                return
            }
            // NOTE:
            // If user lift fingers -> ended
            // If gesture.isEnabled -> cancelled
            
            // Ended, Animate back to start
            dismissalAnimator!.pauseAnimation()
            dismissalAnimator!.isReversed = true
            
            // Disable gesture until reverse closing animation finishes.
            gesture.isEnabled = false
            dismissalAnimator!.addCompletion { [unowned self] (pos) in
                self.didCancelDismissalTransition()
                gesture.isEnabled = true
            }
            dismissalAnimator!.startAnimation()
        default:
            fatalError("Impossible gesture state? \(gesture.state.rawValue)")
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if draggingDownToDismiss || (scrollView.isTracking && scrollView.contentOffset.y < 0) {
            
            scrollView.contentOffset = .zero
        }
        
        scrollView.showsVerticalScrollIndicator = !draggingDownToDismiss
        
    }
    
    func didCancelDismissalTransition() {
        // Clean up
        interactiveStartingPoint = nil
        dismissalAnimator = nil
        draggingDownToDismiss = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 120, right: 0)
        
        dismissalPanGesture.addTarget(self, action: #selector(handleDismissalPan(gesture:)))
        dismissalPanGesture.delegate = self
        
        dismissalScreenEdgePanGesture.addTarget(self, action: #selector(handleDismissalPan(gesture:)))
        dismissalScreenEdgePanGesture.delegate = self
        
        // Make drag down/scroll pan gesture waits til screen edge pan to fail first to begin
        dismissalPanGesture.require(toFail: dismissalScreenEdgePanGesture)
        tableView.panGestureRecognizer.require(toFail: dismissalScreenEdgePanGesture)
        
        loadViewIfNeeded()
        view.addGestureRecognizer(dismissalPanGesture)
        view.addGestureRecognizer(dismissalScreenEdgePanGesture)
        
        
        setupTable()
        accessURL2()
       
        self.loadingProgress(progress: 30)
        
        accessURL()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if #available(iOS 11.0, *) {
            // viewDidLayoutSubviewsではSafeAreaの取得ができている
          
            bottomSafeAreaHeight = self.view.safeAreaInsets.bottom
          
        
            print(bottomSafeAreaHeight) // iPhoneXなら34,  その他は0
            
            let width:CGFloat = self.view.frame.width
            let height:CGFloat = self.view.frame.height
            let labelHeight:CGFloat = 50
           
            buttomButton.frame = CGRect(
                x: 0, y: height - (labelHeight + bottomSafeAreaHeight),
                width: width, height: labelHeight)
        }
    }
    
    func accessURL(){
        
        
        if let sentData = appDelegate.setData{
            // 取得したJSONを格納する変数を定義
            var getJson: NSDictionary!
            
            // API接続先
            let urlStr = "\(Api.url.apiUrl):\(Api.url.apiPort)/v2/customer/item/detail?auth=a3e4e8f1a6637836ab960454828aec5ce241fa0946fb55cee78963d47ed182be&user_id=0000227a-900d-46e3-bad2-9baafa43fcb5&action=4&lang=2&item_id=\(sentData.0)&app_version=3.4.4"
            
            if let url = URL(string: urlStr) {
                let req = NSMutableURLRequest(url: url)
                req.httpMethod = "GET"
                // req.httpBody = "userId=\(self.userId)&code=\(self.code)".data(using: String.Encoding.utf8)
                let task = URLSession.shared.dataTask(with: req as URLRequest, completionHandler: { (data, resp, err) in
                    print(resp!.url!)
                    print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as Any)
                    
                    // 受け取ったdataをJSONパース、エラーならcatchへジャンプ
                    do {
                        
                        
                        
                        getJson = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary

                        let jsonIp = (getJson["item"] as? NSDictionary)!
                        
                        var name = (jsonIp["name"] as? String)!
                        if !(name is NSNull){
                            
                        }else{
                            name = "未設定"
                        }
                        var title = (jsonIp["title"] as? String)!
                        if !(title is NSNull){
                            
                        }else{
                            title = "未設定"
                        }
                        
                        
                        var detail = (jsonIp["detail"] as? String)!
                        if !(detail is NSNull){
                            
                        }else{
                            detail = "未設定"
                        }
                        
                        var category = (jsonIp["category"])!
                        if !(category is NSNull){
                            
                        }else{
                            category = "未設定"
                        }
                        
                        var like_count = (jsonIp["like_count"])!
                        if !(like_count is NSNull){
                            
                        }else{
                            like_count = 0
                        }
                        
                        var star_avg:Double = (jsonIp["star_avg"])! as! Double
                        if !(star_avg is NSNull){
                            
                        }else{
                            star_avg = 0.0
                        }
                        
                        var maker = (jsonIp["maker"])
                        
                        if !(maker is NSNull){
                            
                        }else{
                            maker = "未設定"
                        }
                        
                        var spec_title = (jsonIp["spec_title"])
                        
                        if !(spec_title is NSNull){
                            
                        }else{
                            spec_title = "未設定"
                        }
                        
                        var rankingArray = (getJson["ranking"] as? NSDictionary)!
                        
                        print("djj \(rankingArray)")
                        
                        var ranking = (rankingArray["ranking"] as? Int)!
                        if !(ranking is NSNull){
                            
                        }else{
                            ranking = 0
                        }
                        
                        var count = (rankingArray["count"] as? Int)!
                        if !(count is NSNull){
                            
                        }else{
                            count = 0
                        }
                        
                        
                        let items = (jsonIp["images"]) as? [[String: AnyObject]]
                        
                        
                        var Array:[String] = []
                        
                        for item in items! {
                            
                            let i = item["url"]
                            Array.append(i as! String)
                        }
                        
                        
                        
                        DispatchQueue.main.async{
                            
                            self.item = ItemModel(title: name, subTitle: title, maker: maker as! String, category: category as! String, rate: star_avg, likeCount: like_count as! Int, imgUrl:Array, spec_title: spec_title as! String, ranking: ranking, count: count)
                            
                            self.loadData(str: detail)
                            
                            self.tableView.reloadData()
                        }
                        
                    } catch {
                        print ("json error")
                        return
                    }
                })
                task.resume()
            }
            
            
        }else{
        
        }
    }
    
    
    func accessURL2(){
        
        
        if let sentData = appDelegate.setData{
            // 取得したJSONを格納する変数を定義
            var getJson: NSDictionary!
            
            // API接続先
           
            let urlStr = "\(Api.url.apiUrl):\(Api.url.apiPort)/v2/customer/item/reviews?" + "&" + "item_id=\(sentData.0)" + "&" + "item_review_id=0" + "&" + "review_count=100" + "&" + "auth=\(Api.url.auth)" + "&" + "&user_id=\(Api.url.userId)" + "&" + "&lang=1"
            

            
            if let url = URL(string: urlStr) {
                let req = NSMutableURLRequest(url: url)
                req.httpMethod = "GET"
                // req.httpBody = "userId=\(self.userId)&code=\(self.code)".data(using: String.Encoding.utf8)
                let task = URLSession.shared.dataTask(with: req as URLRequest, completionHandler: { (data, resp, err) in
                    print(resp!.url!)
                    print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as Any)
                    
                    // 受け取ったdataをJSONパース、エラーならcatchへジャンプ
                    do {
                        let myGroup = DispatchGroup()
                        print("🆗あかきくけこ \(data!)")
                        
                        getJson = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                       
                        
                        
                           print("ddkkwsdawdaajs \(getJson)")
                       
                        var arr:Array = ((getJson["reviews"] as! NSArray) as Array)
            
                        for i in arr{
                            
                            myGroup.enter()
                            
                            self.commentItem.append(CommentModel(item_review_id: self.getJsonStr(ob: i as AnyObject, name: "item_review_id"),
                                                                 star: self.getJsonStr(ob: i as AnyObject, name: "star"),
                                                                 gender: self.getJsonStr(ob: i as AnyObject, name: "gender"),
                                                                 age: self.getJsonStr(ob: i as AnyObject, name: "age"),
                                                                 review_date: self.getJsonStr(ob: i as AnyObject, name: "review_date"),
                                                                 review: self.getJsonStr(ob: i as AnyObject, name: "review"),
                                                                 user_country: self.getJsonStr(ob: i as AnyObject, name: "user_country"),
                                                                 like_comment: self.getJsonStr(ob: i as AnyObject, name: "like_comment"),
                                                                 still_like_comment: self.getJsonStr(ob: i as AnyObject, name: "still_like_comment"),
                                                                 username: self.getJsonStr(ob: i as AnyObject, name: "username"),
                                                                 image_profile: self.getJsonStr(ob: i as AnyObject, name: "image_profile"),
                                                                 good: self.getUserStarJsonStr(ob: i as AnyObject, name: "good"),
                                                                 normal: self.getUserStarJsonStr(ob: i as AnyObject, name: "normal"),
                                                                 bad: self.getUserStarJsonStr(ob: i as AnyObject, name: "bad"),
                                                                 user_star_avg: self.getJsonStr(ob: i as AnyObject, name: "user_star_avg")))
                            
                              myGroup.leave()
                            
                             print("COMMENN \(self.commentItem)")
                        }
                        myGroup.notify(queue: .main) {
                            self.tableView.reloadData()
                        }
                        
                    } catch {
                        print ("json error")
                        return
                    }
                })
                task.resume()
            }
            
            
        }else{
            
        }
    }
    
    
    func getJsonStr(ob:AnyObject, name:String) -> String{
        
        
        if let title = ob["\(name)"] as? String
        {
            return title
        }
        else
        {
            return ""
        }
    }
    
    func getUserStarJsonStr(ob:AnyObject, name:String) -> String{
        
        if let json_hobby = ob["user_star"] as? NSDictionary {
            var count = (json_hobby[name])!
            if !(count is NSNull){
                print("🍴 \(name) _ \(count)")
                 return "\(count)"
            }else{
                return "0"
            }
        }else{
            return "0"
        }


    }
    
    func setupTable() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 300
        tableView.tableFooterView = UIView()
    }
    
    func loadData(str:String) {
        
        //loading html
        let html = """
        <HTML>
        <HEAD>
        <TITLE>Your Title Here</TITLE>
        </HEAD>
        <BODY BGCOLOR="FFFFFF">
        \(str)
        <br><br><br><br>
        
        </BODY>
        </HTML>
        """
        progressView.isHidden = false
        progressView.setProgress(0, animated: false)
        htmlCell.htmlView.html = html
        
        //adding custom script - action when image clicked
        //adding observer and handling callback in delegate method handleScriptMessage
        let script = "var imgElement = document.getElementById(\"myImage\"); imgElement.onclick = function(e) { window.webkit.messageHandlers.\(PSHTMLViewScriptMessage.HandlerName.onImageClicked.rawValue).postMessage(e.currentTarget.getAttribute(\"src\")); };"
        htmlCell.htmlView.addScript(script, observeMessageWithName: .onImageClicked)
    }
    
}

extension PSHTMLInTableViewController : UITableViewDataSource, UITableViewDelegate  {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3 + self.commentItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "DetailHeaderTableViewCell", for: indexPath) as! DetailHeaderTableViewCell
            
            //cell.delegate = self

            if let date = self.item?.imgUrl{
                cell.item = self.item
            }
            
            
            return cell
        case 1:
            return htmlCell
        
        case 2:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentSetTableViewCell", for: indexPath) as! CommentSetTableViewCell
          
            cell.item = self.item
            
            cell.delegate = self
            
            return cell
        
        default:
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell", for: indexPath) as! CommentTableViewCell
            
            print("commjh \(self.commentItem.count)  \(indexPath.row)")
            
            if self.commentItem.count >= indexPath.row{
                 cell.commentItem = self.commentItem[indexPath.row - 1]
            }
            
           
            cell.delegate = self
            return cell
        }
    }
    
}

extension PSHTMLInTableViewController: PSHTMLViewDelegate {
    func shouldNavigate(for navigationAction: WKNavigationAction) -> Bool {
        if navigationAction.navigationType == .linkActivated, let url = navigationAction.request.url {
            //example of intercepting link and launching ios mail app (should work on real device)
            if url.absoluteString.hasPrefix("mailto:") {
                if MFMailComposeViewController.canSendMail() {
                    //todo send mail
                    let composeVC = MFMailComposeViewController()
                    composeVC.mailComposeDelegate = self
                    if let recipient = url.absoluteString.components(separatedBy: ":").last {
                        composeVC.setToRecipients([recipient])
                    }
                    composeVC.setSubject("Hello!")
                    composeVC.setMessageBody("Hello, this webview was useful!", isHTML: false)
                    self.present(composeVC, animated: true, completion: nil)
                } else {
                    //example of calling javascript from swift (note that alert shown is native one tnx to WKUIDelegate, will show in simulator)
                    htmlCell.htmlView.webView.evaluateJavaScript("alert(\"Mail services are not available\");")
                }
                return false
            }
            //intentinally openining all other links in Safari
            let svc = SFSafariViewController(url: url)
            present(svc, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    func presentAlert(_ alertController: UIAlertController) {
        present(alertController, animated: true)
    }
    
    func heightChanged(height: CGFloat) {
        print(height)
        tableView.reloadData()
    }
    
    func handleScriptMessage(_ message: WKScriptMessage) {
        //opening image link in safari when clicked
        if message.name == PSHTMLViewScriptMessage.HandlerName.onImageClicked.rawValue {
            if let urlString = message.body as? String, let url = URL(string: urlString) {
                let svc = SFSafariViewController(url: url)
                present(svc, animated: true, completion: nil)
            }
        }
    }
    
    func loadingProgress(progress: Float) {
        progressView.isHidden = progress == 1
        progressView.setProgress(progress, animated: true)
    }
    
    func didFinishLoad() {
        progressView.setProgress(0, animated: false)
    }
    
}

extension PSHTMLInTableViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

extension PSHTMLViewScriptMessage.HandlerName {
    static let onImageClicked =  PSHTMLViewScriptMessage.HandlerName("onImageClicked")
}

extension PSHTMLInTableViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension PSHTMLInTableViewController: CommentSetTableViewCellDelegate {
    func starSelected(star: Int) {
        
        // Create a custom view controller
        let ratingVC = RatingViewController(nibName: "RatingViewController", bundle: nil)
        
        // Create the dialog
        let popup = PopupDialog(viewController: ratingVC,
                                buttonAlignment: .horizontal,
                                transitionStyle: .bounceDown,
                                tapGestureDismissal: true,
                                panGestureDismissal: true)
        
        // Create first button
        let buttonOne = CancelButton(title: "CANCEL", height: 60) {
            
        }
        
        // Create second button
        let buttonTwo = DefaultButton(title: "RATE", height: 60) {
            
        }
        
        // Add buttons to dialog
        popup.addButtons([buttonOne, buttonTwo])
        
        // Present dialog
        present(popup, animated: true, completion: nil)
    }
    

}


extension PSHTMLInTableViewController: CommentTableViewCellDelegate {
    func tappedReport(id: String) {
        //        ① コントローラーの実装
        let alertController = UIAlertController(title: "違反",message: "ID:\(id)", preferredStyle: UIAlertController.Style.alert)
        
        //        ②-1 OKボタンの実装
        let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default){ (action: UIAlertAction) in
            //        ②-2 OKがクリックされた時の処理
            print("Hello")
        }
        //        CANCELボタンの実装
        let cancelButton = UIAlertAction(title: "CANCEL", style: UIAlertAction.Style.cancel, handler: nil)
        
        //        ③-1 ボタンに追加
        alertController.addAction(okAction)
        //        ③-2 CANCELボタンの追加
        alertController.addAction(cancelButton)
        
        //        ④ アラートの表示
        present(alertController,animated: true,completion: nil)
    }
    
   
    
    
}
