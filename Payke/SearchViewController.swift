//
//  SearchViewController.swift
//  Payke
//
//  Created by ShinjiYamamoto on 2019/04/13.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//

import UIKit
import FittedSheets

class SearchViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    
    let userDefaults = UserDefaults.standard
    var initBool = false
    private var selectedImageView: UIImageView?
    @IBOutlet weak var tableView: UITableView!
    let searchBar = UISearchBar()
    let statusBarHeight = UIApplication.shared.statusBarFrame.height
    var textFieldHeight: CGFloat = 40
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.tintColor = #colorLiteral(red: 0, green: 0.686277926, blue: 0.9266666174, alpha: 1)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor(white: 0.9, alpha: 1)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        searchBar.sizeToFit()
        searchBar.placeholder = "商品を検索"
        searchBar.delegate = self
        self.initBool = false
        searchBar.showsCancelButton = true
        self.navigationController?.navigationBar.topItem?.titleView = searchBar
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text! = ""
        self.tableView.reloadData()
        searchBar.resignFirstResponder()
        searchBar.showsCancelButton  = false
        self.initBool = false
        self.tableView.reloadData()
    }
    
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    
    // 新しく履歴に追加
    func addHistory(text: String) {
        if text == "" {
            return
        }
        
        var histories = getInputHistory()
        
        for word in histories {
            if word == text {
                // すでに履歴にある場合は追加しない
                return
            }
        }
        histories.insert(text, at: 0)
        userDefaults.set(histories, forKey: "inputHistory")
    }
    
    // 履歴を一つ削除
    func removeHistory(index: Int) {
        var histories = getInputHistory()
        histories.remove(at: index)
        userDefaults.set(histories, forKey: "inputHistory")
    }
    
    // 履歴取得
    func getInputHistory() -> [String] {
        if let histories = userDefaults.array(forKey: "inputHistory") as? [String] {
            return histories
        }
        return []
    }
    
    // UITableViewCellから履歴を入力
    @objc func inputFromHistory(sender: UITapGestureRecognizer) {
        if let cell = sender.view as? UITableViewCell {
            searchBar.text = cell.textLabel?.text
            self.initBool = true
            self.tableView.reloadData()
        }
    }
    
    // MARK: - UITextFieldDelegate関連
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.searchBar.resignFirstResponder()
        addHistory(text: self.searchBar.text!)
        self.searchBar.text = ""
        self.tableView.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchBar.text)
    }
    
    func searchBarCancelButtonClicked(searchBar: UISearchBar) {
        // Stop doing the search stuff
        // and clear the text in the search bar
        searchBar.text = ""
        // Hide the cancel button
        searchBar.showsCancelButton = false
        self.initBool = false
        self.tableView.reloadData()
        // You could also change the position, frame etc of the searchBar
    }
    
    // MARK: - UITableView関連
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // テーブルセルの数を設定（必須）
        
        if self.initBool == false{
            return getInputHistory().count
        }else{
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // テーブルセルを作成（必須）
        
        if self.initBool == false{
            
            let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
            cell.textLabel?.text = getInputHistory()[indexPath.row]
            cell.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.inputFromHistory(sender:))))
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchItemTableViewCell", for: indexPath) as! SearchItemTableViewCell
            
            cell.delegate = self
           // let item = itemsArray[indexPath.row]
          
            
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            removeHistory(index: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    func tableView(_ tableView: UITableView, titleForDeleteConfirmationButtonForRowAt indexPath: IndexPath) -> String? {
        // 左スワイプして出てくる削除ボタンのテキスト
        return "削除"
    }
    
    private func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCell.EditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        // 左スワイプして出てくる削除ボタンを押した時の処理
        
        removeHistory(index: indexPath.row)
        
        tableView.deleteRows(at: [indexPath as IndexPath], with: .fade)
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // すべてのセルを削除可能に
        return true
    }
}


extension SearchViewController: SearchItemTableViewCellDelegate {
    
    
    func goToZoom(itemview: UIImageView, item: ItemModel) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.item = item
        
//        let story = UIStoryboard(name: "Home", bundle: nil)
//        let vc = story.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
//        vc.img = itemview.image!
//        selectedImageView = itemview
//        vc.cc_setZoomTransition(originalView: itemview)
//        self.present(vc, animated: true, completion: nil)
    }
    
}
