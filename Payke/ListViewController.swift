
//  Payke
//
//  Created by ShinjiYamamoto on 2019/04/14.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//

import UIKit
import FittedSheets

class ListViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    
    var itemsArray:[ListModel] = []
    
    override func viewDidAppear(_ animated: Bool) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.tableView.reloadData()
        }
    }
    
    
    private var selectedImageView: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reloadData()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 60 // 初期値なんでもいい
        tableView.rowHeight = UITableView.automaticDimension // 高さ自動設定
        
        print("💤２２２””” \(itemsArray)")
    }
    
    func reloadData(){
        
    
        
        var url = "\(Api.url.apiUrl):\(Api.url.apiPort)/v2/customer/item/like_list?"
            + "&auth=\(Api.url.auth)"
            + "&user_id=\(Api.url.userId)"
            + "&lang=2"
        
        var getJson: NSDictionary!
        
        if let url = URL(string: url) {
            
            print ("☔️ \(url)")
            
            let req = NSMutableURLRequest(url: url)
            req.httpMethod = "GET"
            // req.httpBody = "userId=\(self.userId)&code=\(self.code)".data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: req as URLRequest, completionHandler: { (data, resp, err) in
                //                    print(resp!.url!)
                //                    print(NSString(data: data!, encoding: Str ing.Encoding.utf8.rawValue) as Any)
                
                // 受け取ったdataをJSONパース、エラーな らcatchへジャンプ
                
                print("😷2")
                
                do {
                    
                    print("😷3")
                    
                    
                    getJson = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    
                    print ("☔️1 \(getJson!)")
                    
                    var arr:Array = ((getJson["items"] as! NSArray) as Array)
                    print ("arr💤 \(arr)")
                    let myGroup = DispatchGroup()
                    
                    for y in arr{
                        print ("ardwar💤 \(y)")
                        
                        myGroup.enter()
                        
                        
                        
                        let data = ListModel(category: self.getJsonStr(ob: y, name: "category"),
                                             category_id: self.getJsonStr(ob: y, name: "category_id"),
                                             id: self.getJsonStr(ob: y, name: "id"),
                                             image: self.getJsonStr(ob: y, name: "image"),
                                             like_count: self.getJsonInt(ob: y, name: "like_count"),
                                             name: self.getJsonStr(ob: y, name: "name"),
                                             review_count: self.getJsonInt(ob: y, name: "review_count"),
                                             star_avg: self.getJsonDouble(ob: y, name: "star_avg"),
                                             title: self.getJsonStr(ob: y, name: "title"))
                        self.itemsArray.append(data)
                        myGroup.leave()
                    }
                    
                    myGroup.notify(queue: .main) {
                        self.tableView.reloadData()
                    }
                    
                    
                    
                    print ("💭💤")
                    
                    
                    
                    
                } catch {
                    print ("json error")
                    print("😷4")
                    return
                }
            })
            task.resume()
        }
    }
    
    func getJsonImg(ob:AnyObject, name:String) -> String{
        
        if let title = ob["\(name)"] as? [String: Any]
        {
            if let url:String = title["url"] as! String{
                return url
            }else{
                return ""
            }
        }
        else
        {
            return ""
        }
    }
    
    func getJsonStr(ob:AnyObject, name:String) -> String{
        
        
        if let value = ob["\(name)"] as? String
        {
            return value
        }
        else
        {
            return ""
        }
    }
	
	func getJsonInt(ob:AnyObject, name:String) -> Int{
		
		
		if let value = ob["\(name)"] as? Int
		{
			return value
		}
		else
		{
			return 9999
		}
	}
	
	func getJsonDouble(ob:AnyObject, name:String) -> Double{
		
		
		if let value = ob["\(name)"] as? Double
		{
			return value
		}
		else
		{
			return 0.00001
		}
	}
	
	
	
	
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell", for: indexPath) as! ListTableViewCell
            let item = itemsArray[indexPath.row]
            cell.item = item
            cell.delegate = self
            cell.noView.layer.borderColor = UIColor.yellow.cgColor
            cell.noView.layer.borderWidth = 3
            cell.noLabel.text! = "\(indexPath.row + 1)"
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell", for: indexPath) as! ListTableViewCell
            let item = itemsArray[indexPath.row]
            cell.item = item
            cell.delegate = self
            cell.noView.layer.borderColor = UIColor.gray.cgColor
            cell.noView.layer.borderWidth = 3
            cell.noLabel.text! = "\(indexPath.row + 1)"
            
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell", for: indexPath) as! ListTableViewCell
            let item = itemsArray[indexPath.row]
            cell.item = item
            cell.delegate = self
            cell.noView.layer.borderColor = UIColor.brown.cgColor
            cell.noView.layer.borderWidth = 3
            cell.noLabel.text! = "\(indexPath.row + 1)"
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell", for: indexPath) as! ListTableViewCell
            let item = itemsArray[indexPath.row]
            cell.item = item
            cell.delegate = self
            cell.noView.layer.borderColor = UIColor.gray.cgColor
            cell.noView.layer.borderWidth = 1
            cell.noLabel.text! = "\(indexPath.row + 1)"
            
            
            return cell
        }
    }
    
}
extension ListViewController: ListTableViewCellDelegate {
    
    
    func goToZoom(itemview: UIImageView, no: String) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.setData = (no,1)
        
        //performSegue(withIdentifier: "segue", sender: nil)
        let controller = SheetViewController(controller: UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "PSHTMLInTableViewController"), sizes: [.fullScreen])
        
        self.present(controller, animated: false, completion: nil)
        
    }
    
    
}
