//
//  DetailColumnViewController.swift
//  Payke
//
//  Created by ShinjiYamamoto on 2019/05/24.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//

import UIKit
import PSHTMLView
import SafariServices
import WebKit
import MessageUI


class PSHTMLCell2: UITableViewCell {
    @IBOutlet weak var htmlView : PSHTMLView!
    
    
}

class DetailColumnViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet weak var  progressView: UIProgressView!
    @IBOutlet weak var tableView: UITableView!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    
    lazy private var htmlCell: PSHTMLCell2 = { [weak self] in
        var cell = self?.tableView.dequeueReusableCell(withIdentifier: "HTMLCell") as! PSHTMLCell2
        cell.htmlView.delegate = self
        return cell
        }()
    
    var draggingDownToDismiss = false
    var interactiveStartingPoint: CGPoint?
    var dismissalAnimator: UIViewPropertyAnimator?
    final class DismissalPanGesture: UIPanGestureRecognizer {}
    final class DismissalScreenEdgePanGesture: UIScreenEdgePanGestureRecognizer {}
    
    private lazy var dismissalPanGesture: DismissalPanGesture = {
        let pan = DismissalPanGesture()
        pan.maximumNumberOfTouches = 1
        return pan
    }()
    
    private lazy var dismissalScreenEdgePanGesture: DismissalScreenEdgePanGesture = {
        let pan = DismissalScreenEdgePanGesture()
        pan.edges = .left
        return pan
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.sheetViewController?.handleScrollView(self.tableView)
        
    }
    
    ///////dismise action
    
    func didSuccessfullyDragDownToDismiss() {
        let indexPath = IndexPath(row: 0, section: 0)
        self.tableView.scrollToRow(at: indexPath, at: .top, animated: true)
        self.dismiss(animated: true)
    }
    
    
    // This handles both screen edge and dragdown pan. As screen edge pan is a subclass of pan gesture, this input param works.
    @objc func handleDismissalPan(gesture: UIPanGestureRecognizer) {
        
        let isScreenEdgePan = gesture.isKind(of: DismissalScreenEdgePanGesture.self)
        let canStartDragDownToDismissPan = !isScreenEdgePan && !draggingDownToDismiss
        
        // Don't do anything when it's not in the drag down mode
        if canStartDragDownToDismissPan { return }
        
        let targetAnimatedView = gesture.view!
        let startingPoint: CGPoint
        
        if let p = interactiveStartingPoint {
            startingPoint = p
        } else {
            // Initial location
            startingPoint = gesture.location(in: nil)
            interactiveStartingPoint = startingPoint
        }
        
        let currentLocation = gesture.location(in: nil)
        let progress = isScreenEdgePan ? (gesture.translation(in: targetAnimatedView).x / 100) : (currentLocation.y - startingPoint.y) / 100
        let targetShrinkScale: CGFloat = 0.86
        let targetCornerRadius: CGFloat = GlobalConstants.cardCornerRadius
        
        func createInteractiveDismissalAnimatorIfNeeded() -> UIViewPropertyAnimator {
            if let animator = dismissalAnimator {
                return animator
            } else {
                let animator = UIViewPropertyAnimator(duration: 0, curve: .linear, animations: {
                    targetAnimatedView.transform = .init(scaleX: targetShrinkScale, y: targetShrinkScale)
                    targetAnimatedView.layer.cornerRadius = targetCornerRadius
                })
                animator.isReversed = false
                animator.pauseAnimation()
                animator.fractionComplete = progress
                return animator
            }
        }
        
        switch gesture.state {
        case .began:
            dismissalAnimator = createInteractiveDismissalAnimatorIfNeeded()
            
        case .changed:
            dismissalAnimator = createInteractiveDismissalAnimatorIfNeeded()
            
            let actualProgress = progress
            let isDismissalSuccess = actualProgress >= 1.0
            
            dismissalAnimator!.fractionComplete = actualProgress
            
            if isDismissalSuccess {
                dismissalAnimator!.stopAnimation(false)
                dismissalAnimator!.addCompletion { (pos) in
                    switch pos {
                    case .end:
                        self.didSuccessfullyDragDownToDismiss()
                    default:
                        fatalError("Must finish dismissal at end!")
                    }
                }
                dismissalAnimator!.finishAnimation(at: .end)
            }
            
        case .ended, .cancelled:
            if dismissalAnimator == nil {
                // Gesture's too quick that it doesn't have dismissalAnimator!
                print("Too quick there's no animator!")
                didCancelDismissalTransition()
                return
            }
            // NOTE:
            // If user lift fingers -> ended
            // If gesture.isEnabled -> cancelled
            
            // Ended, Animate back to start
            dismissalAnimator!.pauseAnimation()
            dismissalAnimator!.isReversed = true
            
            // Disable gesture until reverse closing animation finishes.
            gesture.isEnabled = false
            dismissalAnimator!.addCompletion { [unowned self] (pos) in
                self.didCancelDismissalTransition()
                gesture.isEnabled = true
            }
            dismissalAnimator!.startAnimation()
        default:
            fatalError("Impossible gesture state? \(gesture.state.rawValue)")
        }
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if draggingDownToDismiss || (scrollView.isTracking && scrollView.contentOffset.y < 0) {
            
            scrollView.contentOffset = .zero
        }
        
        scrollView.showsVerticalScrollIndicator = !draggingDownToDismiss
    }
    
    func didCancelDismissalTransition() {
        // Clean up
        interactiveStartingPoint = nil
        dismissalAnimator = nil
        draggingDownToDismiss = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dismissalPanGesture.addTarget(self, action: #selector(handleDismissalPan(gesture:)))
        dismissalPanGesture.delegate = self
        
        dismissalScreenEdgePanGesture.addTarget(self, action: #selector(handleDismissalPan(gesture:)))
        dismissalScreenEdgePanGesture.delegate = self
        
        // Make drag down/scroll pan gesture waits til screen edge pan to fail first to begin
        dismissalPanGesture.require(toFail: dismissalScreenEdgePanGesture)
        tableView.panGestureRecognizer.require(toFail: dismissalScreenEdgePanGesture)
        
        loadViewIfNeeded()
        view.addGestureRecognizer(dismissalPanGesture)
        view.addGestureRecognizer(dismissalScreenEdgePanGesture)
        
        
    
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        accessURL()
        // Do any additional setup after loading the view.
    }
    
    
    func accessURL(){
        
        
        
        if let sentData = appDelegate.columnItem{
            // 取得したJSONを格納する変数を定義
            var getJson: NSDictionary!
            
            // API接続先
            let urlStr = "https://column.payke.co.jp/wp-admin/admin-ajax.php?action=page&lang=ja&id=\(sentData.id!)&user_id=a0d74dc1-ab0d-4c83-a2eb-c9552b371e13"
            
            if let url = URL(string: urlStr) {
                let req = NSMutableURLRequest(url: url)
                req.httpMethod = "GET"
                // req.httpBody = "userId=\(self.userId)&code=\(self.code)".data(using: String.Encoding.utf8)
                let task = URLSession.shared.dataTask(with: req as URLRequest, completionHandler: { (data, resp, err) in
                    print(resp!.url!)
                    print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as Any)
                    
                    // 受け取ったdataをJSONパース、エラーならcatchへジャンプ
                    do {
                        
                        getJson = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        
                        print("ddkkwdaajs \(getJson)")
                        
                        
                        let jsonIp = (getJson["data"] as? NSDictionary)!
                        
                        var name = (jsonIp["text"] as? String)!
                        if !(name is NSNull){
                            
                        }else{
                            name = "未設定"
                        }
                        
  print ("💤a \(name)")
                        
                     
                        
                        DispatchQueue.main.async{
                               self.loadData(str: name)
                         
                            
                            self.tableView.reloadData()
                        }
                        
                    } catch {
                        print ("json error")
                        return
                    }
                })
                task.resume()
            }
            
            
        }else{
         
        }
    }
    
    
    func setupTable() {
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 300
        tableView.tableFooterView = UIView()
    }
    
    func loadData(str:String) {
        
        //loading html
        let html = """
        <HTML>
        <HEAD>
        <TITLE>Your Title Here</TITLE>
        </HEAD>
        <BODY BGCOLOR="FFFFFF">
        \(str)
        <br><br><br><br>
        
        </BODY>
        </HTML>
        """
        
        htmlCell.htmlView.html = html
        
        //adding custom script - action when image clicked
        //adding observer and handling callback in delegate method handleScriptMessage
        let script = "var imgElement = document.getElementById(\"myImage\"); imgElement.onclick = function(e) { window.webkit.messageHandlers.\(PSHTMLViewScriptMessage.HandlerName.onImageClicked.rawValue).postMessage(e.currentTarget.getAttribute(\"src\")); };"
        htmlCell.htmlView.addScript(script, observeMessageWithName: .onImageClicked)
        
        
        //---- CSS 的応用 ---//
        
//        let path = Bundle.main.path(forResource: "main", ofType: "css")!
//        if let data = NSData(contentsOfFile: path){
//            let stra = String(NSString(data: data as Data, encoding: String.Encoding.utf8.rawValue)!)
//
//        }else{
//            print("データなし")
//        }
//
        
         //---- CSS 的応用 ---//

    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        
        
        
        if indexPath.row == 0{
             let cell = tableView.dequeueReusableCell(withIdentifier: "ColumnHeaderTableViewCell", for: indexPath) as! ColumnHeaderTableViewCell
            
            cell.item = appDelegate.columnItem
            
            return cell
        }else{
            return htmlCell
        }
        
    }


}




extension DetailColumnViewController: PSHTMLViewDelegate {
    func shouldNavigate(for navigationAction: WKNavigationAction) -> Bool {
        if navigationAction.navigationType == .linkActivated, let url = navigationAction.request.url {
            //example of intercepting link and launching ios mail app (should work on real device)
            if url.absoluteString.hasPrefix("mailto:") {
                if MFMailComposeViewController.canSendMail() {
                    //todo send mail
                    let composeVC = MFMailComposeViewController()
                    composeVC.mailComposeDelegate = self
                    if let recipient = url.absoluteString.components(separatedBy: ":").last {
                        composeVC.setToRecipients([recipient])
                    }
                    composeVC.setSubject("Hello!")
                    composeVC.setMessageBody("Hello, this webview was useful!", isHTML: false)
                    self.present(composeVC, animated: true, completion: nil)
                } else {
                    //example of calling javascript from swift (note that alert shown is native one tnx to WKUIDelegate, will show in simulator)
                    htmlCell.htmlView.webView.evaluateJavaScript("alert(\"Mail services are not available\");")
                }
                return false
            }
            //intentinally openining all other links in Safari
            let svc = SFSafariViewController(url: url)
            present(svc, animated: true, completion: nil)
            return false
        }
        return true
    }
    
    func presentAlert(_ alertController: UIAlertController) {
        present(alertController, animated: true)
    }
    
    func heightChanged(height: CGFloat) {
        print(height)
        tableView.reloadData()
    }
    
    func handleScriptMessage(_ message: WKScriptMessage) {
        //opening image link in safari when clicked
        if message.name == PSHTMLViewScriptMessage.HandlerName.onImageClicked.rawValue {
            if let urlString = message.body as? String, let url = URL(string: urlString) {
                let svc = SFSafariViewController(url: url)
                present(svc, animated: true, completion: nil)
            }
        }
    }
    
    func loadingProgress(progress: Float) {
        progressView.isHidden = progress == 1
        progressView.setProgress(progress, animated: true)
    }
    
    func didFinishLoad() {
        progressView.setProgress(0, animated: false)
    }
    
}

extension DetailColumnViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}


extension DetailColumnViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}


