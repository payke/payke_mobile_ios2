
//
//  OverlayChildViewController.swift
//  iOS Sample
//
//  Copyright (c) 2017 Kazuhiro Hayashi
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit
import PagingKit

class OverlayChildViewController: UIViewController {
    
    var items1:[RankingModel] = []
    var items2:[RankingModel] = []
    var items3:[RankingModel] = []
    var items4:[RankingModel] = []
    var items5:[RankingModel] = []
    var items6:[RankingModel] = []
    var items7:[RankingModel] = []
    var items8:[RankingModel] = []
    var items9:[RankingModel] = []
    var items10:[RankingModel] = []
    var items11:[RankingModel] = []
    var items12:[RankingModel] = []
    var items13:[RankingModel] = []
    var items14:[RankingModel] = []
    var items15:[RankingModel] = []
    
    let dataSource: [(menu: String, content: UIViewController)] = ["Around you", "Fit for you", "All", "Food", "Diet/health", "Cosmetics/perfume", "PC/peripheral device", "AV equipment/camera", "home electronics", "Living/daily","Game/toy","Baby","Gift","Medical","Pet supplies, creatures"].map {
        let title = $0
        let story = UIStoryboard(name: "Ranking", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "RankingViewController") as! RankingViewController

        
        return (menu: title, content: vc)
    }
    
    var menuViewController: PagingMenuViewController!
    var contentViewController: PagingContentViewController!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var url = "\(Api.url.apiUrl):\(Api.url.apiPort)/v2/customer/item/ranking_all?" + "auth=\(Api.url.auth)" + "&user_id=\(Api.url.userId)" + "&lang=2"
        
         var getJson: NSDictionary!
        
        if let url = URL(string: url) {
            
            print ("dwa💤 \(url)")
            
            let req = NSMutableURLRequest(url: url)
            req.httpMethod = "GET"
            // req.httpBody = "userId=\(self.userId)&code=\(self.code)".data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: req as URLRequest, completionHandler: { (data, resp, err) in
                //                    print(resp!.url!)
                //                    print(NSString(data: data!, encoding: Str ing.Encoding.utf8.rawValue) as Any)
                
                // 受け取ったdataをJSONパース、エラーな らcatchへジャンプ
                
                print("😷2")
                
                do {
                    
                    print("😷3")
                    
                    
                    getJson = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    
                    print ("💭💤1 \(getJson!)")
                    var arr:Array = ((getJson["all_ranking"] as! NSArray) as Array)
                    print ("asrr💤 \(arr)")
                    
                  
                    
                    for i in arr{
                        
                       
                        
                        var any = i["items"]
                        
                        var ranking_id = i["ranking_id"]
                        
                        
                        var name = (i["ranking_id"])!
                        

                        
                        switch "\(name!)"{
                            
                        case "1002":
                            let myGroup = DispatchGroup()
                            for y in any as! Array<NSDictionary>{
                                
                                myGroup.enter()
                                
                                let data = RankingModel(age: self.getJsonStr(ob: y as AnyObject, name: "age"),
                                                        country: self.getJsonStr(ob: y as AnyObject, name: "country"),
                                                        gender: self.getJsonStr(ob: y as AnyObject, name: "gender"),
                                                        image_1: self.getJsonStr(ob: y as AnyObject, name: "image_1"),
                                                        is_translated: self.getJsonStr(ob: y as AnyObject, name: "is_translated"),
                                                        is_updated: self.getJsonStr(ob: y as AnyObject, name: "is_updated"),
                                                        item_category: self.getJsonStr(ob: y as AnyObject, name: "item_category"),
                                                        item_category_name: self.getJsonStr(ob: y as AnyObject, name: "item_category_name"),
                                                        item_id: self.getJsonStr(ob: y as AnyObject, name: "item_id"),
                                                        last_filter_at: self.getJsonStr(ob: y as AnyObject, name: "last_filter_at"),
                                                        like_count: self.getJsonStr(ob: y as AnyObject, name: "like_count"),
                                                        made_in_japan: self.getJsonStr(ob: y as AnyObject, name: "made_in_japan"),
                                                        name: self.getJsonStr(ob: y as AnyObject, name: "name"),
                                                        rank: self.getJsonStr(ob: y as AnyObject, name: "rank"),
                                                        review_count: self.getJsonStr(ob: y as AnyObject, name: "review_count"),
                                                        star_avg: self.getJsonStr(ob: y as AnyObject, name: "star_avg"),
                                                        thumbnail_url: self.getJsonStr(ob: y as AnyObject, name: "thumbnail_url"),
                                                        title: self.getJsonStr(ob: y as AnyObject, name: "title"))
                                
                                self.items1.append(data)
                                myGroup.leave()

                            }
                            myGroup.notify(queue: .main) {
                                self.contentViewController.reloadData()
                            }
                        case "1001":
                            let myGroup = DispatchGroup()
                            for y in any as! Array<NSDictionary>{
                                
                                myGroup.enter()
                                
                                let data = RankingModel(age: self.getJsonStr(ob: y as AnyObject, name: "age"),
                                                        country: self.getJsonStr(ob: y as AnyObject, name: "country"),
                                                        gender: self.getJsonStr(ob: y as AnyObject, name: "gender"),
                                                        image_1: self.getJsonStr(ob: y as AnyObject, name: "image_1"),
                                                        is_translated: self.getJsonStr(ob: y as AnyObject, name: "is_translated"),
                                                        is_updated: self.getJsonStr(ob: y as AnyObject, name: "is_updated"),
                                                        item_category: self.getJsonStr(ob: y as AnyObject, name: "item_category"),
                                                        item_category_name: self.getJsonStr(ob: y as AnyObject, name: "item_category_name"),
                                                        item_id: self.getJsonStr(ob: y as AnyObject, name: "item_id"),
                                                        last_filter_at: self.getJsonStr(ob: y as AnyObject, name: "last_filter_at"),
                                                        like_count: self.getJsonStr(ob: y as AnyObject, name: "like_count"),
                                                        made_in_japan: self.getJsonStr(ob: y as AnyObject, name: "made_in_japan"),
                                                        name: self.getJsonStr(ob: y as AnyObject, name: "name"),
                                                        rank: self.getJsonStr(ob: y as AnyObject, name: "rank"),
                                                        review_count: self.getJsonStr(ob: y as AnyObject, name: "review_count"),
                                                        star_avg: self.getJsonStr(ob: y as AnyObject, name: "star_avg"),
                                                        thumbnail_url: self.getJsonStr(ob: y as AnyObject, name: "thumbnail_url"),
                                                        title: self.getJsonStr(ob: y as AnyObject, name: "title"))
                                
                                self.items2.append(data)
                                myGroup.leave()
                                
                            }
                            myGroup.notify(queue: .main) {
                                self.contentViewController.reloadData()
                            }
                        case "1":
                            let myGroup = DispatchGroup()
                            for y in any as! Array<NSDictionary>{
                                
                                myGroup.enter()
                                
                                let data = RankingModel(age: self.getJsonStr(ob: y as AnyObject, name: "age"),
                                                        country: self.getJsonStr(ob: y as AnyObject, name: "country"),
                                                        gender: self.getJsonStr(ob: y as AnyObject, name: "gender"),
                                                        image_1: self.getJsonStr(ob: y as AnyObject, name: "image_1"),
                                                        is_translated: self.getJsonStr(ob: y as AnyObject, name: "is_translated"),
                                                        is_updated: self.getJsonStr(ob: y as AnyObject, name: "is_updated"),
                                                        item_category: self.getJsonStr(ob: y as AnyObject, name: "item_category"),
                                                        item_category_name: self.getJsonStr(ob: y as AnyObject, name: "item_category_name"),
                                                        item_id: self.getJsonStr(ob: y as AnyObject, name: "item_id"),
                                                        last_filter_at: self.getJsonStr(ob: y as AnyObject, name: "last_filter_at"),
                                                        like_count: self.getJsonStr(ob: y as AnyObject, name: "like_count"),
                                                        made_in_japan: self.getJsonStr(ob: y as AnyObject, name: "made_in_japan"),
                                                        name: self.getJsonStr(ob: y as AnyObject, name: "name"),
                                                        rank: self.getJsonStr(ob: y as AnyObject, name: "rank"),
                                                        review_count: self.getJsonStr(ob: y as AnyObject, name: "review_count"),
                                                        star_avg: self.getJsonStr(ob: y as AnyObject, name: "star_avg"),
                                                        thumbnail_url: self.getJsonStr(ob: y as AnyObject, name: "thumbnail_url"),
                                                        title: self.getJsonStr(ob: y as AnyObject, name: "title"))
                                
                                self.items3.append(data)
                                myGroup.leave()
                                
                            }
                            myGroup.notify(queue: .main) {
                                self.contentViewController.reloadData()
                            }
                        case "3":
                            let myGroup = DispatchGroup()
                            for y in any as! Array<NSDictionary>{
                                
                                print("yahh \(y)")
                                
                                myGroup.enter()
                                
                                let data = RankingModel(age: self.getJsonStr(ob: y as AnyObject, name: "age"),
                                                        country: self.getJsonStr(ob: y as AnyObject, name: "country"),
                                                        gender: self.getJsonStr(ob: y as AnyObject, name: "gender"),
                                                        image_1: self.getJsonStr(ob: y as AnyObject, name: "image_1"),
                                                        is_translated: self.getJsonStr(ob: y as AnyObject, name: "is_translated"),
                                                        is_updated: self.getJsonStr(ob: y as AnyObject, name: "is_updated"),
                                                        item_category: self.getJsonStr(ob: y as AnyObject, name: "item_category"),
                                                        item_category_name: self.getJsonStr(ob: y as AnyObject, name: "item_category_name"),
                                                        item_id: self.getJsonStr(ob: y as AnyObject, name: "item_id"),
                                                        last_filter_at: self.getJsonStr(ob: y as AnyObject, name: "last_filter_at"),
                                                        like_count: self.getJsonStr(ob: y as AnyObject, name: "like_count"),
                                                        made_in_japan: self.getJsonStr(ob: y as AnyObject, name: "made_in_japan"),
                                                        name: self.getJsonStr(ob: y as AnyObject, name: "name"),
                                                        rank: self.getJsonStr(ob: y as AnyObject, name: "rank"),
                                                        review_count: self.getJsonStr(ob: y as AnyObject, name: "review_count"),
                                                        star_avg: self.getJsonStr(ob: y as AnyObject, name: "star_avg"),
                                                        thumbnail_url: self.getJsonStr(ob: y as AnyObject, name: "thumbnail_url"),
                                                        title: self.getJsonStr(ob: y as AnyObject, name: "title"))
                                
                                self.items4.append(data)
                                myGroup.leave()
                                
                            }
                            myGroup.notify(queue: .main) {
                                self.contentViewController.reloadData()
                            }
                        case "5":
                            let myGroup = DispatchGroup()
                            for y in any as! Array<NSDictionary>{
                                
                                myGroup.enter()
                                
                                let data = RankingModel(age: self.getJsonStr(ob: y as AnyObject, name: "age"),
                                                        country: self.getJsonStr(ob: y as AnyObject, name: "country"),
                                                        gender: self.getJsonStr(ob: y as AnyObject, name: "gender"),
                                                        image_1: self.getJsonStr(ob: y as AnyObject, name: "image_1"),
                                                        is_translated: self.getJsonStr(ob: y as AnyObject, name: "is_translated"),
                                                        is_updated: self.getJsonStr(ob: y as AnyObject, name: "is_updated"),
                                                        item_category: self.getJsonStr(ob: y as AnyObject, name: "item_category"),
                                                        item_category_name: self.getJsonStr(ob: y as AnyObject, name: "item_category_name"),
                                                        item_id: self.getJsonStr(ob: y as AnyObject, name: "item_id"),
                                                        last_filter_at: self.getJsonStr(ob: y as AnyObject, name: "last_filter_at"),
                                                        like_count: self.getJsonStr(ob: y as AnyObject, name: "like_count"),
                                                        made_in_japan: self.getJsonStr(ob: y as AnyObject, name: "made_in_japan"),
                                                        name: self.getJsonStr(ob: y as AnyObject, name: "name"),
                                                        rank: self.getJsonStr(ob: y as AnyObject, name: "rank"),
                                                        review_count: self.getJsonStr(ob: y as AnyObject, name: "review_count"),
                                                        star_avg: self.getJsonStr(ob: y as AnyObject, name: "star_avg"),
                                                        thumbnail_url: self.getJsonStr(ob: y as AnyObject, name: "thumbnail_url"),
                                                        title: self.getJsonStr(ob: y as AnyObject, name: "title"))
                                
                                self.items5.append(data)
                                myGroup.leave()
                                
                            }
                            myGroup.notify(queue: .main) {
                                self.contentViewController.reloadData()
                            }
                        case "6":
                            let myGroup = DispatchGroup()
                            for y in any as! Array<NSDictionary>{
                                
                                myGroup.enter()
                                
                                let data = RankingModel(age: self.getJsonStr(ob: y as AnyObject, name: "age"),
                                                        country: self.getJsonStr(ob: y as AnyObject, name: "country"),
                                                        gender: self.getJsonStr(ob: y as AnyObject, name: "gender"),
                                                        image_1: self.getJsonStr(ob: y as AnyObject, name: "image_1"),
                                                        is_translated: self.getJsonStr(ob: y as AnyObject, name: "is_translated"),
                                                        is_updated: self.getJsonStr(ob: y as AnyObject, name: "is_updated"),
                                                        item_category: self.getJsonStr(ob: y as AnyObject, name: "item_category"),
                                                        item_category_name: self.getJsonStr(ob: y as AnyObject, name: "item_category_name"),
                                                        item_id: self.getJsonStr(ob: y as AnyObject, name: "item_id"),
                                                        last_filter_at: self.getJsonStr(ob: y as AnyObject, name: "last_filter_at"),
                                                        like_count: self.getJsonStr(ob: y as AnyObject, name: "like_count"),
                                                        made_in_japan: self.getJsonStr(ob: y as AnyObject, name: "made_in_japan"),
                                                        name: self.getJsonStr(ob: y as AnyObject, name: "name"),
                                                        rank: self.getJsonStr(ob: y as AnyObject, name: "rank"),
                                                        review_count: self.getJsonStr(ob: y as AnyObject, name: "review_count"),
                                                        star_avg: self.getJsonStr(ob: y as AnyObject, name: "star_avg"),
                                                        thumbnail_url: self.getJsonStr(ob: y as AnyObject, name: "thumbnail_url"),
                                                        title: self.getJsonStr(ob: y as AnyObject, name: "title"))
                                
                                self.items6.append(data)
                                myGroup.leave()
                                
                            }
                            myGroup.notify(queue: .main) {
                                self.contentViewController.reloadData()
                            }
                        case "7":
                            let myGroup = DispatchGroup()
                            for y in any as! Array<NSDictionary>{
                                
                                myGroup.enter()
                                
                                let data = RankingModel(age: self.getJsonStr(ob: y as AnyObject, name: "age"),
                                                        country: self.getJsonStr(ob: y as AnyObject, name: "country"),
                                                        gender: self.getJsonStr(ob: y as AnyObject, name: "gender"),
                                                        image_1: self.getJsonStr(ob: y as AnyObject, name: "image_1"),
                                                        is_translated: self.getJsonStr(ob: y as AnyObject, name: "is_translated"),
                                                        is_updated: self.getJsonStr(ob: y as AnyObject, name: "is_updated"),
                                                        item_category: self.getJsonStr(ob: y as AnyObject, name: "item_category"),
                                                        item_category_name: self.getJsonStr(ob: y as AnyObject, name: "item_category_name"),
                                                        item_id: self.getJsonStr(ob: y as AnyObject, name: "item_id"),
                                                        last_filter_at: self.getJsonStr(ob: y as AnyObject, name: "last_filter_at"),
                                                        like_count: self.getJsonStr(ob: y as AnyObject, name: "like_count"),
                                                        made_in_japan: self.getJsonStr(ob: y as AnyObject, name: "made_in_japan"),
                                                        name: self.getJsonStr(ob: y as AnyObject, name: "name"),
                                                        rank: self.getJsonStr(ob: y as AnyObject, name: "rank"),
                                                        review_count: self.getJsonStr(ob: y as AnyObject, name: "review_count"),
                                                        star_avg: self.getJsonStr(ob: y as AnyObject, name: "star_avg"),
                                                        thumbnail_url: self.getJsonStr(ob: y as AnyObject, name: "thumbnail_url"),
                                                        title: self.getJsonStr(ob: y as AnyObject, name: "title"))
                                
                                self.items7.append(data)
                                myGroup.leave()
                                
                            }
                            myGroup.notify(queue: .main) {
                                self.contentViewController.reloadData()
                            }
                        case "8":
                            let myGroup = DispatchGroup()
                            for y in any as! Array<NSDictionary>{
                                
                                myGroup.enter()
                                
                                let data = RankingModel(age: self.getJsonStr(ob: y as AnyObject, name: "age"),
                                                        country: self.getJsonStr(ob: y as AnyObject, name: "country"),
                                                        gender: self.getJsonStr(ob: y as AnyObject, name: "gender"),
                                                        image_1: self.getJsonStr(ob: y as AnyObject, name: "image_1"),
                                                        is_translated: self.getJsonStr(ob: y as AnyObject, name: "is_translated"),
                                                        is_updated: self.getJsonStr(ob: y as AnyObject, name: "is_updated"),
                                                        item_category: self.getJsonStr(ob: y as AnyObject, name: "item_category"),
                                                        item_category_name: self.getJsonStr(ob: y as AnyObject, name: "item_category_name"),
                                                        item_id: self.getJsonStr(ob: y as AnyObject, name: "item_id"),
                                                        last_filter_at: self.getJsonStr(ob: y as AnyObject, name: "last_filter_at"),
                                                        like_count: self.getJsonStr(ob: y as AnyObject, name: "like_count"),
                                                        made_in_japan: self.getJsonStr(ob: y as AnyObject, name: "made_in_japan"),
                                                        name: self.getJsonStr(ob: y as AnyObject, name: "name"),
                                                        rank: self.getJsonStr(ob: y as AnyObject, name: "rank"),
                                                        review_count: self.getJsonStr(ob: y as AnyObject, name: "review_count"),
                                                        star_avg: self.getJsonStr(ob: y as AnyObject, name: "star_avg"),
                                                        thumbnail_url: self.getJsonStr(ob: y as AnyObject, name: "thumbnail_url"),
                                                        title: self.getJsonStr(ob: y as AnyObject, name: "title"))
                                
                                self.items8.append(data)
                                myGroup.leave()
                                
                            }
                            myGroup.notify(queue: .main) {
                                self.contentViewController.reloadData()
                            }
                        case "9":
                            
                            let myGroup = DispatchGroup()
                            for y in any as! Array<NSDictionary>{
                                
                                myGroup.enter()
                                
                                let data = RankingModel(age: self.getJsonStr(ob: y as AnyObject, name: "age"),
                                                        country: self.getJsonStr(ob: y as AnyObject, name: "country"),
                                                        gender: self.getJsonStr(ob: y as AnyObject, name: "gender"),
                                                        image_1: self.getJsonStr(ob: y as AnyObject, name: "image_1"),
                                                        is_translated: self.getJsonStr(ob: y as AnyObject, name: "is_translated"),
                                                        is_updated: self.getJsonStr(ob: y as AnyObject, name: "is_updated"),
                                                        item_category: self.getJsonStr(ob: y as AnyObject, name: "item_category"),
                                                        item_category_name: self.getJsonStr(ob: y as AnyObject, name: "item_category_name"),
                                                        item_id: self.getJsonStr(ob: y as AnyObject, name: "item_id"),
                                                        last_filter_at: self.getJsonStr(ob: y as AnyObject, name: "last_filter_at"),
                                                        like_count: self.getJsonStr(ob: y as AnyObject, name: "like_count"),
                                                        made_in_japan: self.getJsonStr(ob: y as AnyObject, name: "made_in_japan"),
                                                        name: self.getJsonStr(ob: y as AnyObject, name: "name"),
                                                        rank: self.getJsonStr(ob: y as AnyObject, name: "rank"),
                                                        review_count: self.getJsonStr(ob: y as AnyObject, name: "review_count"),
                                                        star_avg: self.getJsonStr(ob: y as AnyObject, name: "star_avg"),
                                                        thumbnail_url: self.getJsonStr(ob: y as AnyObject, name: "thumbnail_url"),
                                                        title: self.getJsonStr(ob: y as AnyObject, name: "title"))
                                
                                self.items9.append(data)
                                myGroup.leave()
                                
                            }
                            myGroup.notify(queue: .main) {
                                self.contentViewController.reloadData()
                            }
                        case "12":
                            let myGroup = DispatchGroup()
                            for y in any as! Array<NSDictionary>{
                                
                                myGroup.enter()
                                
                                let data = RankingModel(age: self.getJsonStr(ob: y as AnyObject, name: "age"),
                                                        country: self.getJsonStr(ob: y as AnyObject, name: "country"),
                                                        gender: self.getJsonStr(ob: y as AnyObject, name: "gender"),
                                                        image_1: self.getJsonStr(ob: y as AnyObject, name: "image_1"),
                                                        is_translated: self.getJsonStr(ob: y as AnyObject, name: "is_translated"),
                                                        is_updated: self.getJsonStr(ob: y as AnyObject, name: "is_updated"),
                                                        item_category: self.getJsonStr(ob: y as AnyObject, name: "item_category"),
                                                        item_category_name: self.getJsonStr(ob: y as AnyObject, name: "item_category_name"),
                                                        item_id: self.getJsonStr(ob: y as AnyObject, name: "item_id"),
                                                        last_filter_at: self.getJsonStr(ob: y as AnyObject, name: "last_filter_at"),
                                                        like_count: self.getJsonStr(ob: y as AnyObject, name: "like_count"),
                                                        made_in_japan: self.getJsonStr(ob: y as AnyObject, name: "made_in_japan"),
                                                        name: self.getJsonStr(ob: y as AnyObject, name: "name"),
                                                        rank: self.getJsonStr(ob: y as AnyObject, name: "rank"),
                                                        review_count: self.getJsonStr(ob: y as AnyObject, name: "review_count"),
                                                        star_avg: self.getJsonStr(ob: y as AnyObject, name: "star_avg"),
                                                        thumbnail_url: self.getJsonStr(ob: y as AnyObject, name: "thumbnail_url"),
                                                        title: self.getJsonStr(ob: y as AnyObject, name: "title"))
                                
                                self.items10.append(data)
                                myGroup.leave()
                                
                            }
                            myGroup.notify(queue: .main) {
                                self.contentViewController.reloadData()
                            }
                        case "15":
                            let myGroup = DispatchGroup()
                            for y in any as! Array<NSDictionary>{
                                
                                myGroup.enter()
                                
                                let data = RankingModel(age: self.getJsonStr(ob: y as AnyObject, name: "age"),
                                                        country: self.getJsonStr(ob: y as AnyObject, name: "country"),
                                                        gender: self.getJsonStr(ob: y as AnyObject, name: "gender"),
                                                        image_1: self.getJsonStr(ob: y as AnyObject, name: "image_1"),
                                                        is_translated: self.getJsonStr(ob: y as AnyObject, name: "is_translated"),
                                                        is_updated: self.getJsonStr(ob: y as AnyObject, name: "is_updated"),
                                                        item_category: self.getJsonStr(ob: y as AnyObject, name: "item_category"),
                                                        item_category_name: self.getJsonStr(ob: y as AnyObject, name: "item_category_name"),
                                                        item_id: self.getJsonStr(ob: y as AnyObject, name: "item_id"),
                                                        last_filter_at: self.getJsonStr(ob: y as AnyObject, name: "last_filter_at"),
                                                        like_count: self.getJsonStr(ob: y as AnyObject, name: "like_count"),
                                                        made_in_japan: self.getJsonStr(ob: y as AnyObject, name: "made_in_japan"),
                                                        name: self.getJsonStr(ob: y as AnyObject, name: "name"),
                                                        rank: self.getJsonStr(ob: y as AnyObject, name: "rank"),
                                                        review_count: self.getJsonStr(ob: y as AnyObject, name: "review_count"),
                                                        star_avg: self.getJsonStr(ob: y as AnyObject, name: "star_avg"),
                                                        thumbnail_url: self.getJsonStr(ob: y as AnyObject, name: "thumbnail_url"),
                                                        title: self.getJsonStr(ob: y as AnyObject, name: "title"))
                                
                                self.items11.append(data)
                                myGroup.leave()
                                
                            }
                            myGroup.notify(queue: .main) {
                                self.contentViewController.reloadData()
                            }
                        case "16":
                            let myGroup = DispatchGroup()
                            for y in any as! Array<NSDictionary>{
                                
                                myGroup.enter()
                                
                                let data = RankingModel(age: self.getJsonStr(ob: y as AnyObject, name: "age"),
                                                        country: self.getJsonStr(ob: y as AnyObject, name: "country"),
                                                        gender: self.getJsonStr(ob: y as AnyObject, name: "gender"),
                                                        image_1: self.getJsonStr(ob: y as AnyObject, name: "image_1"),
                                                        is_translated: self.getJsonStr(ob: y as AnyObject, name: "is_translated"),
                                                        is_updated: self.getJsonStr(ob: y as AnyObject, name: "is_updated"),
                                                        item_category: self.getJsonStr(ob: y as AnyObject, name: "item_category"),
                                                        item_category_name: self.getJsonStr(ob: y as AnyObject, name: "item_category_name"),
                                                        item_id: self.getJsonStr(ob: y as AnyObject, name: "item_id"),
                                                        last_filter_at: self.getJsonStr(ob: y as AnyObject, name: "last_filter_at"),
                                                        like_count: self.getJsonStr(ob: y as AnyObject, name: "like_count"),
                                                        made_in_japan: self.getJsonStr(ob: y as AnyObject, name: "made_in_japan"),
                                                        name: self.getJsonStr(ob: y as AnyObject, name: "name"),
                                                        rank: self.getJsonStr(ob: y as AnyObject, name: "rank"),
                                                        review_count: self.getJsonStr(ob: y as AnyObject, name: "review_count"),
                                                        star_avg: self.getJsonStr(ob: y as AnyObject, name: "star_avg"),
                                                        thumbnail_url: self.getJsonStr(ob: y as AnyObject, name: "thumbnail_url"),
                                                        title: self.getJsonStr(ob: y as AnyObject, name: "title"))
                                
                                self.items12.append(data)
                                myGroup.leave()
                                
                            }
                            myGroup.notify(queue: .main) {
                                self.contentViewController.reloadData()
                            }
                        case "23":
                            let myGroup = DispatchGroup()
                            for y in any as! Array<NSDictionary>{
                                
                                myGroup.enter()
                                
                                let data = RankingModel(age: self.getJsonStr(ob: y as AnyObject, name: "age"),
                                                        country: self.getJsonStr(ob: y as AnyObject, name: "country"),
                                                        gender: self.getJsonStr(ob: y as AnyObject, name: "gender"),
                                                        image_1: self.getJsonStr(ob: y as AnyObject, name: "image_1"),
                                                        is_translated: self.getJsonStr(ob: y as AnyObject, name: "is_translated"),
                                                        is_updated: self.getJsonStr(ob: y as AnyObject, name: "is_updated"),
                                                        item_category: self.getJsonStr(ob: y as AnyObject, name: "item_category"),
                                                        item_category_name: self.getJsonStr(ob: y as AnyObject, name: "item_category_name"),
                                                        item_id: self.getJsonStr(ob: y as AnyObject, name: "item_id"),
                                                        last_filter_at: self.getJsonStr(ob: y as AnyObject, name: "last_filter_at"),
                                                        like_count: self.getJsonStr(ob: y as AnyObject, name: "like_count"),
                                                        made_in_japan: self.getJsonStr(ob: y as AnyObject, name: "made_in_japan"),
                                                        name: self.getJsonStr(ob: y as AnyObject, name: "name"),
                                                        rank: self.getJsonStr(ob: y as AnyObject, name: "rank"),
                                                        review_count: self.getJsonStr(ob: y as AnyObject, name: "review_count"),
                                                        star_avg: self.getJsonStr(ob: y as AnyObject, name: "star_avg"),
                                                        thumbnail_url: self.getJsonStr(ob: y as AnyObject, name: "thumbnail_url"),
                                                        title: self.getJsonStr(ob: y as AnyObject, name: "title"))
                                
                                self.items13.append(data)
                                myGroup.leave()
                                
                            }
                            myGroup.notify(queue: .main) {
                                self.contentViewController.reloadData()
                            }
                        case "24":
                            let myGroup = DispatchGroup()
                            for y in any as! Array<NSDictionary>{
                                
                                myGroup.enter()
                                
                                let data = RankingModel(age: self.getJsonStr(ob: y as AnyObject, name: "age"),
                                                        country: self.getJsonStr(ob: y as AnyObject, name: "country"),
                                                        gender: self.getJsonStr(ob: y as AnyObject, name: "gender"),
                                                        image_1: self.getJsonStr(ob: y as AnyObject, name: "image_1"),
                                                        is_translated: self.getJsonStr(ob: y as AnyObject, name: "is_translated"),
                                                        is_updated: self.getJsonStr(ob: y as AnyObject, name: "is_updated"),
                                                        item_category: self.getJsonStr(ob: y as AnyObject, name: "item_category"),
                                                        item_category_name: self.getJsonStr(ob: y as AnyObject, name: "item_category_name"),
                                                        item_id: self.getJsonStr(ob: y as AnyObject, name: "item_id"),
                                                        last_filter_at: self.getJsonStr(ob: y as AnyObject, name: "last_filter_at"),
                                                        like_count: self.getJsonStr(ob: y as AnyObject, name: "like_count"),
                                                        made_in_japan: self.getJsonStr(ob: y as AnyObject, name: "made_in_japan"),
                                                        name: self.getJsonStr(ob: y as AnyObject, name: "name"),
                                                        rank: self.getJsonStr(ob: y as AnyObject, name: "rank"),
                                                        review_count: self.getJsonStr(ob: y as AnyObject, name: "review_count"),
                                                        star_avg: self.getJsonStr(ob: y as AnyObject, name: "star_avg"),
                                                        thumbnail_url: self.getJsonStr(ob: y as AnyObject, name: "thumbnail_url"),
                                                        title: self.getJsonStr(ob: y as AnyObject, name: "title"))
                                
                                self.items14.append(data)
                                myGroup.leave()
                                
                            }
                            myGroup.notify(queue: .main) {
                                self.contentViewController.reloadData()
                            }
                        case "25":
                            let myGroup = DispatchGroup()
                            for y in any as! Array<NSDictionary>{
                                
                                myGroup.enter()
                                
                                let data = RankingModel(age: self.getJsonStr(ob: y as AnyObject, name: "age"),
                                                        country: self.getJsonStr(ob: y as AnyObject, name: "country"),
                                                        gender: self.getJsonStr(ob: y as AnyObject, name: "gender"),
                                                        image_1: self.getJsonStr(ob: y as AnyObject, name: "image_1"),
                                                        is_translated: self.getJsonStr(ob: y as AnyObject, name: "is_translated"),
                                                        is_updated: self.getJsonStr(ob: y as AnyObject, name: "is_updated"),
                                                        item_category: self.getJsonStr(ob: y as AnyObject, name: "item_category"),
                                                        item_category_name: self.getJsonStr(ob: y as AnyObject, name: "item_category_name"),
                                                        item_id: self.getJsonStr(ob: y as AnyObject, name: "item_id"),
                                                        last_filter_at: self.getJsonStr(ob: y as AnyObject, name: "last_filter_at"),
                                                        like_count: self.getJsonStr(ob: y as AnyObject, name: "like_count"),
                                                        made_in_japan: self.getJsonStr(ob: y as AnyObject, name: "made_in_japan"),
                                                        name: self.getJsonStr(ob: y as AnyObject, name: "name"),
                                                        rank: self.getJsonStr(ob: y as AnyObject, name: "rank"),
                                                        review_count: self.getJsonStr(ob: y as AnyObject, name: "review_count"),
                                                        star_avg: self.getJsonStr(ob: y as AnyObject, name: "star_avg"),
                                                        thumbnail_url: self.getJsonStr(ob: y as AnyObject, name: "thumbnail_url"),
                                                        title: self.getJsonStr(ob: y as AnyObject, name: "title"))
                                
                                self.items15.append(data)
                                myGroup.leave()
                                
                            }
                            myGroup.notify(queue: .main) {
                                self.contentViewController.reloadData()
                            }
                            
                            
                        default: break
                           
                        }
                        


                    }
                    
                   
                    print ("💭💤")
                    
                    

                    
                } catch {
                    print ("json error")
                    print("😷4")
                    return
                }
            })
            task.resume()
        }
        
        
        
        menuViewController?.register(nib: UINib(nibName: "OverlayMenuCell", bundle: nil), forCellWithReuseIdentifier: "identifier")
        menuViewController?.registerFocusView(nib: UINib(nibName: "OverlayFocusView", bundle: nil), isBehindCell: true)
        menuViewController?.reloadData(with: 0, completionHandler: { [weak self, menuViewController = menuViewController!] (vc) in
            let cell = self?.menuViewController.currentFocusedCell as! OverlayMenuCell
            cell.setFrame(menuViewController.menuView, maskFrame: menuViewController.focusView.frame, animated: false)
        })
        contentViewController?.scrollView.bounces = true
        contentViewController?.reloadData(with: 0)
    }
    

    
    func getJsonStr(ob:AnyObject, name:String) -> String{

        
        if let title = ob["\(name)"] as? String
        {
            return title
        }
        else
        {
            return ""
        }
    }
    
    func getJsonInt(ob:AnyObject, name:String) -> Int{
        var name = (ob["\(name)"] as? Int)!
        if !(name is NSNull){
            return name
        }else{
            return 999
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PagingMenuViewController {
            menuViewController = vc
            menuViewController?.dataSource = self
            menuViewController?.delegate = self
            menuViewController?.cellAlignment = .center
        } else if let vc = segue.destination as? PagingContentViewController {
            contentViewController = vc
            contentViewController?.delegate = self
            contentViewController?.dataSource = self
            menuViewController?.cellAlignment = .center
        }
    }
}

extension OverlayChildViewController: PagingMenuViewControllerDataSource {
    func menuViewController(viewController: PagingMenuViewController, cellForItemAt index: Int) -> PagingMenuViewCell {
        let cell = viewController.dequeueReusableCell(withReuseIdentifier: "identifier", for: index)  as! OverlayMenuCell
        cell.titleLabel.text = dataSource[index].menu
        
        cell.setFrame(viewController.menuView, maskFrame: menuViewController.focusView.frame, animated: false)
        return cell
    }
    
    
    func menuViewController(viewController: PagingMenuViewController, widthForItemAt index: Int) -> CGFloat {
        OverlayMenuCell.sizingCell.titleLabel.text = dataSource[index].menu
        var referenceSize = UIView.layoutFittingCompressedSize
        referenceSize.height = viewController.view.bounds.height
        let size = OverlayMenuCell.sizingCell.systemLayoutSizeFitting(referenceSize, withHorizontalFittingPriority: UILayoutPriority.defaultLow, verticalFittingPriority: UILayoutPriority.defaultHigh)
        return size.width
    }
    
    func numberOfItemsForMenuViewController(viewController: PagingMenuViewController) -> Int {
        return dataSource.count
    }
}


extension OverlayChildViewController: PagingContentViewControllerDataSource {
    func numberOfItemsForContentViewController(viewController: PagingContentViewController) -> Int {
        return dataSource.count
    }
    
    func contentViewController(viewController: PagingContentViewController, viewControllerAt index: Int) -> UIViewController {
        
        let vc = dataSource[index].content as! RankingViewController
        
  
        
        switch index{
        case 0:
            vc.itemsArray = items1
        case 1:
            vc.itemsArray = items2
        case 2:
            vc.itemsArray = items3
        case 3:
            vc.itemsArray = items4
        case 4:
            vc.itemsArray = items5
        case 5:
            vc.itemsArray = items6
        case 6:
            vc.itemsArray = items7
        case 7:
            vc.itemsArray = items8
        case 8:
            vc.itemsArray = items9
        case 9:
            vc.itemsArray = items10
        case 10:
            vc.itemsArray = items11
        case 11:
            vc.itemsArray = items12
        case 12:
            vc.itemsArray = items13
        case 13:
            vc.itemsArray = items14
        case 14:
            vc.itemsArray = items15
            
        default:
           print("")
        }
        
        
        
        return dataSource[index].content
    }
}

extension OverlayChildViewController: PagingMenuViewControllerDelegate {
    func menuViewController(viewController: PagingMenuViewController, didSelect page: Int, previousPage: Int) {
        contentViewController?.scroll(to: page, animated: true)
    }
    
    func menuViewController(viewController: PagingMenuViewController, willAnimateFocusViewTo index: Int, with coordinator: PagingMenuFocusViewAnimationCoordinator) {
        viewController.visibleCells.compactMap { $0 as? OverlayMenuCell }.forEach { cell in
            cell.setFrame(viewController.menuView, maskFrame: coordinator.beginFrame, animated: true)
        }
        
        coordinator.animateFocusView(alongside: { coordinator in
            viewController.visibleCells.compactMap { $0 as? OverlayMenuCell }.forEach { cell in
                cell.setFrame(viewController.menuView, maskFrame: coordinator.endFrame, animated: true)
            }
        }, completion: nil)
    }
    
    func menuViewController(viewController: PagingMenuViewController, willDisplay cell: PagingMenuViewCell, forItemAt index: Int) {
        (cell as? OverlayMenuCell)?.setFrame(viewController.menuView, maskFrame: menuViewController.focusView.frame, animated: false)
    }
}

extension OverlayChildViewController: PagingContentViewControllerDelegate {
    func contentViewController(viewController: PagingContentViewController, didManualScrollOn index: Int, percent: CGFloat) {
        menuViewController.scroll(index: index, percent: percent, animated: false)
        menuViewController.visibleCells.forEach {
            let cell = $0 as! OverlayMenuCell
            cell.setFrame(menuViewController.menuView, maskFrame: menuViewController.focusView.frame, animated: false)
        }
    }
}
