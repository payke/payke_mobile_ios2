//
//  DetailHeaderTableViewCell.swift
//  Payke
//
//  Created by ShinjiYamamoto on 2019/04/15.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//

import UIKit
import ZKCarousel

class DetailHeaderTableViewCell: UITableViewCell {
    


 
    @IBOutlet weak var likeCountLabel: UILabel!
    
    @IBOutlet weak var carouselImageView: ZKCarousel!
    
    @IBOutlet weak var subImage1: UIImageView!
    @IBOutlet weak var subImage2: UIImageView!
    @IBOutlet weak var subImage3: UIImageView!
    @IBOutlet weak var subImage4: UIImageView!
    @IBOutlet weak var makerLabel: UILabel!
    @IBOutlet weak var makerSubLabel: UILabel!
    @IBOutlet weak var mainNameLabel: UILabel!
    @IBOutlet weak var subLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    
    @IBOutlet weak var avg_cosmos: CosmosView!
    @IBOutlet weak var avg_cosmos_label: UILabel!
    @IBOutlet weak var rankingLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    
    var disabledHighlightedAnimation = false
    weak var delegate: RankingTableViewCellDelegate?
    
    var item: ItemModel? {
        didSet {
            self.updateView()
        }
    }
    
    @objc func goToDetail(){
        // self.delegate?.goToDetail(item: item!)
    }
    
    @objc func goToPage1(){
        self.carouselImageView.tapImageHandler(no:0)
    }
    
    @objc func goToPage2(){
        self.carouselImageView.tapImageHandler(no:1)
    }
    
    @objc func goToPage3(){
        self.carouselImageView.tapImageHandler(no:2)
    }
    
    @objc func goToPage4(){
        self.carouselImageView.tapImageHandler(no:3)
    }
    
    func updateView() {
        
        self.rankingLabel.text! = "\((self.item?.ranking)!)/\((self.item?.count)!)"
        self.categoryLabel.text! = "(\((self.item?.category)!))"
        
        
        if item?.rate == nil{
            self.avg_cosmos.rating = 0.0
            self.avg_cosmos_label.text! = "0"
        }else{
            self.avg_cosmos.rating = self.item!.rate
            self.avg_cosmos_label.text! = "\(self.item!.rate)"
        }
        
        self.mainNameLabel.text! = self.item!.title
        self.subLabel.text! = self.item!.subTitle
        self.makerLabel.text! = self.item!.maker
        avg_cosmos.settings.updateOnTouch = false
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(self.goToPage1))
        self.subImage1.addGestureRecognizer(tapGesture1)
        self.subImage1.isUserInteractionEnabled = true
        
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(self.goToPage2))
        self.subImage2.addGestureRecognizer(tapGesture2)
        self.subImage2.isUserInteractionEnabled = true
        
        let tapGesture3 = UITapGestureRecognizer(target: self, action: #selector(self.goToPage3))
        self.subImage3.addGestureRecognizer(tapGesture3)
        self.subImage3.isUserInteractionEnabled = true
        
        let tapGesture4 = UITapGestureRecognizer(target: self, action: #selector(self.goToPage4))
        self.subImage4.addGestureRecognizer(tapGesture4)
        self.subImage4.isUserInteractionEnabled = true
        
        switch (self.item?.imgUrl.count)! {
        case 1:
            self.subImage1.isHidden = false
            self.subImage2.isHidden = true
            self.subImage3.isHidden = true
            self.subImage4.isHidden = true
            
            let slide = ZKCarouselSlide(image: (self.item?.imgUrl[0])!,title: "", description: "")
            Api.Img.configureCell(with: item!.imgUrl[0], imageView: self.subImage1)
            self.carouselImageView.slides = [slide]
        case 2:
            self.subImage1.isHidden = false
            self.subImage2.isHidden = false
            self.subImage3.isHidden = true
            self.subImage4.isHidden = true
            
            let slide = ZKCarouselSlide(image: (self.item?.imgUrl[0])!,title: "", description: "")
            let slide1 = ZKCarouselSlide(image: (self.item?.imgUrl[1])!, title: "", description: "")
            
            Api.Img.configureCell(with: item!.imgUrl[0], imageView: self.subImage1)
            Api.Img.configureCell(with: item!.imgUrl[1], imageView: self.subImage2)
            self.carouselImageView.slides = [slide, slide1]
        case 3:
            self.subImage1.isHidden = false
            self.subImage2.isHidden = false
            self.subImage3.isHidden = false
            self.subImage4.isHidden = true
            
            let slide = ZKCarouselSlide(image: (self.item?.imgUrl[0])!,title: "", description: "")
            let slide1 = ZKCarouselSlide(image: (self.item?.imgUrl[1])!, title: "", description: "")
            let slide2 = ZKCarouselSlide(image:(self.item?.imgUrl[2])!, title: "", description: "")
            
            Api.Img.configureCell(with: item!.imgUrl[0], imageView: self.subImage1)
            Api.Img.configureCell(with: item!.imgUrl[1], imageView: self.subImage2)
            Api.Img.configureCell(with: item!.imgUrl[2], imageView: self.subImage3)
            
            self.carouselImageView.slides = [slide, slide1, slide2]
        default:
            self.subImage1.isHidden = false
            self.subImage2.isHidden = false
            self.subImage3.isHidden = false
            self.subImage4.isHidden = false
            
            let slide = ZKCarouselSlide(image: (self.item?.imgUrl[0])!,title: "", description: "")
            let slide1 = ZKCarouselSlide(image: (self.item?.imgUrl[1])!, title: "", description: "")
            let slide2 = ZKCarouselSlide(image:(self.item?.imgUrl[2])!, title: "", description: "")
            let slide3 = ZKCarouselSlide(image:(self.item?.imgUrl[3])!, title: "", description: "")
            
            Api.Img.configureCell(with: item!.imgUrl[0], imageView: self.subImage1)
            Api.Img.configureCell(with: item!.imgUrl[1], imageView: self.subImage2)
            Api.Img.configureCell(with: item!.imgUrl[2], imageView: self.subImage3)
            Api.Img.configureCell(with: item!.imgUrl[3], imageView: self.subImage4)
            
            self.carouselImageView.slides = [slide, slide1, slide2,slide3]
        }
        
        
        
        //        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.goToDetail))
        //        self.contentView.addGestureRecognizer(tapGesture)
        //        self.contentView.isUserInteractionEnabled = true
        
        
    
        self.likeCountLabel.text! = "\(item!.likeCount)"
        
    }
    
    
}


extension String {
    func htmlToAttributedString(family: String?, size: CGFloat) -> NSAttributedString? {
        do {
            let htmlCSSString = "<style>" +
                "html *" +
                "{" +
                "font-size: \(size)pt !important;" +
                "font-family: \(family ?? "Helvetica"), Helvetica !important;" +
            "}</style> \(self)"
            
            guard let data = htmlCSSString.data(using: String.Encoding.utf8) else {
                return nil
            }
            
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
}
