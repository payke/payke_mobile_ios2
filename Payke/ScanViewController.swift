



import UIKit
import BarcodeScanner
import FittedSheets


final class ScanViewController: UIViewController {
    
    
    
  
    
    @IBAction func handleScannerPresent(_ sender: Any, forEvent event: UIEvent) {
        let viewController = makeBarcodeScannerViewController()
        viewController.title = "Barcode Scanner"
        present(viewController, animated: true, completion: nil)
    }
    
    @IBAction func handleScannerPush(_ sender: Any, forEvent event: UIEvent) {
        let viewController = makeBarcodeScannerViewController()
        viewController.title = "Barcode Scanner"
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.tintColor = #colorLiteral(red: 0, green: 0.686277926, blue: 0.9266666174, alpha: 1)
    }
    
    
    override func viewDidLoad() {
        
        
        let viewController = makeBarcodeScannerViewController()
        viewController.title = "Barcode Scanner"
        viewController.navigationItem.hidesBackButton = true
        navigationController?.pushViewController(viewController, animated: false)
    }
    
    private func makeBarcodeScannerViewController() -> BarcodeScannerViewController {
        let viewController = BarcodeScannerViewController()
        viewController.codeDelegate = self
        viewController.errorDelegate = self
        viewController.dismissalDelegate = self
        return viewController
    }
}

// MARK: - BarcodeScannerCodeDelegate

extension ScanViewController: BarcodeScannerCodeDelegate {
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        print("Barcode Data: \(code)")
        print("Symbology Type: \(type)")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            
            controller.reset()
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate

            appDelegate.setData = (code,1)
            
            let controller = SheetViewController(controller: UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "PSHTMLInTableViewController"), sizes: [.fullScreen])
            
            self.present(controller, animated: false, completion: nil)
        }
        
        
    }
}

// MARK: - BarcodeScannerErrorDelegate

extension ScanViewController: BarcodeScannerErrorDelegate {
    func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
        print(error)
    }
}

// MARK: - BarcodeScannerDismissalDelegate

extension ScanViewController: BarcodeScannerDismissalDelegate {
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        
        navigationController?.popViewController(animated: true)
    }
}
