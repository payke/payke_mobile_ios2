
import UIKit

struct ListModel {
    
    let category: String
    let category_id: String
    let id: String
    let image: String
    let like_count: Int
    let name: String
    let review_count: Int
    let star_avg: Double
    let title: String
}
