import UIKit

struct RankingModel {
    
    let age: String
    let country: String
    let gender: String
    let image_1: String
    let is_translated: String
    let is_updated: String
    let item_category: String
    let item_category_name: String
    let item_id: String
    let last_filter_at: String
    let like_count: String
    let made_in_japan: String
    let name: String
    let rank: String
    let review_count: String
    let star_avg: String
    let thumbnail_url: String
    let title: String
}
