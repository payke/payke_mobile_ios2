//
//  TimelineModel.swift
//  Payke
//
//  Created by ShinjiYamamoto on 2019/05/31.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//

import Foundation

struct TimelineModel {
    
    let age: String
    let content_score: String
    let created_at: String
    let display_type: String
    let id: String
    let is_item_liked: String
    let item_desc_title: String
    let item_id: String
    let item_image: String
    let item_like_count: String
    let item_name: String
    let item_review_qty: String
    let item_star_avg: String
    let item_type: String
    let review_text: String
    let review_username: String
    let scan_count: String
    let scan_user_image: String
    let scan_username: String
    let sex: String
    let user_id: String
    let review_user_id: String
    let review_user_image: String
    let item_category_name: String
    let like_user_image: String
    let comment_like_count: String
    let shareitemlist_id: String
    let like_user_id: String
    let like_username: String
    let shareitem_like_count: String
    let shareitem_title: String
    let comment_user_id: String
    let comment_username: String
    let comment_user_image: String
    let comment_user_message: String
    let user_uploaded_filename: String
    let original_item_id: String
    let original_shareitem_filename: String
    let comment_message: String
    let shareitem_concept: String
    let image_1: String
    let trid: String
    let element_id: String
    let post_title: String
    let post_like_count: String
    let post_content: String
    let review_id: String
    let star_avg: String
    let is_comment_liked: String
    let image: String
    

}







































