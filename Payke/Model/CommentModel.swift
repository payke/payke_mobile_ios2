//
//  CommentModel.swift
//  Payke
//
//  Created by ShinjiYamamoto on 2019/05/29.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//

import UIKit



struct CommentModel {
    var item_review_id: String?
    var star: String?
    var gender: String?
    var age: String?
    var review_date: String?
    var review: String?
    var user_country: String?
    var like_comment: String?
    var still_like_comment: String?
    var username: String?
    var image_profile: String?
    var good: String?
    var normal: String?
    var bad: String?
    var user_star_avg: String?
}

