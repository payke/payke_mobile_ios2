
//
//  StoreModek.swift
//  Payke
//
//  Created by ShinjiYamamoto on 2019/06/27.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//



import UIKit

struct StoreModel {
    
    let address: String
    let category_id: String
    let distance: String
    let lat: String
    let lng: String
    let name: String
}
