
import UIKit

struct ItemModel {

    let title: String
    let subTitle: String
    let maker: String
    let category: String
    let rate: Double
    let likeCount: Int
    let imgUrl: [String]
    let spec_title: String
    let ranking: Int
    let count: Int
}
