//
//  BaseTabbarViewController.swift
//  Payke
//
//  Created by ShinjiYamamoto on 2019/04/13.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//

import UIKit


class BaseTabbarViewController: UITabBarController , UITabBarControllerDelegate {
    
    var didViewBool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.selectedIndex = 0
    }

    internal func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {

        if viewController.restorationIdentifier == "ScanViewController"{
            if  self.didViewBool == false{
                self.didViewBool = true
                 return true
            }else{
                 return false
            }
        }else{
          self.didViewBool = false
        }
        
          return true
    }
}
