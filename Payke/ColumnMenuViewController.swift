



import UIKit
import PagingKit

class ColumnMenuViewController: UIViewController {
    
    let dataSource: [(menu: String, content: UIViewController)] = ["あなたにぴったり", "ランキング", "ALL", "お土産", "グルメ", "健康", "季節", "日本文化", "生活雑貨", "美容・コスメ"].map {
        let title = $0
        let story = UIStoryboard(name: "Home", bundle: nil)
        let vc = story.instantiateViewController(withIdentifier: "columnViewController") as! columnViewController
        
        switch $0 {
        case "ALL":
            vc.category = 0
        case "お土産":
            vc.category = 66
        case "グルメ":
            vc.category = 45
        case "健康":
            vc.category = 80
        case "季節":
            vc.category = 87
        case "日本文化":
            vc.category = 73
        case "生活雑貨":
            vc.category = 59
        case "美容・コスメ":
            vc.category = 52
            
        default:
            print("")
        }
        return (menu: title, content: vc)
    }
    
    var menuViewController: PagingMenuViewController!
    var contentViewController: PagingContentViewController!
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var item:[ColumnModel]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuViewController?.register(nib: UINib(nibName: "OverlayMenuCell", bundle: nil), forCellWithReuseIdentifier: "identifier")
        menuViewController?.registerFocusView(nib: UINib(nibName: "OverlayFocusView", bundle: nil), isBehindCell: true)
        menuViewController?.reloadData(with: 0, completionHandler: { [weak self, menuViewController = menuViewController!] (vc) in
            let cell = self?.menuViewController.currentFocusedCell as! OverlayMenuCell
            cell.setFrame(menuViewController.menuView, maskFrame: menuViewController.focusView.frame, animated: false)
        })
        contentViewController?.scrollView.bounces = true
        contentViewController?.reloadData(with: 0)
        accessURL()
    }
    
    
    func accessURL(){
        
        var CastAnotherThemeList: [Any] = []
        var getJson: NSDictionary!
        // API接続先 ALL
        let urlStr = "https://column.payke.co.jp/wp-admin/admin-ajax.php?action=category&lang=ja"
        
        if let url = URL(string: urlStr) {
            
            let req = NSMutableURLRequest(url: url)
            req.httpMethod = "GET"

            let task = URLSession.shared.dataTask(with: req as URLRequest, completionHandler: { (data, resp, err) in
                do {
                    
                    getJson = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    
                    var arr:Array = ((getJson["data"] as! NSArray) as Array)

                    for i in arr{
                        
                        self.item?.append(ColumnModel(id: Api.json.getJsonInt(ob: i, name: "id"),
                                                      title: Api.json.getJsonStr(ob: i, name: "title"),
                                                      author: Api.json.getJsonStr(ob: i, name: "author"),
                                                      trid: Api.json.getJsonInt(ob: i, name: "trid"),
                                                      ad: Api.json.getJsonInt(ob: i, name: "ad"),
                                                      like_count: Api.json.getJsonStr(ob: i, name: "like_count"),
                                                      date_unix: Api.json.getJsonInt(ob: i, name: "date_unix"),
                                                      slug: "", url: "", width: "", height: ""))
                        
                    }
                    
                } catch {
                    return
                }
            })
            task.resume()
        }
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? PagingMenuViewController {
            menuViewController = vc
            menuViewController?.dataSource = self
            menuViewController?.delegate = self
            menuViewController?.cellAlignment = .center
        } else if let vc = segue.destination as? PagingContentViewController {
            contentViewController = vc
            contentViewController?.delegate = self
            contentViewController?.dataSource = self
            menuViewController?.cellAlignment = .center
        }
    }
}

extension ColumnMenuViewController: PagingMenuViewControllerDataSource {
    func menuViewController(viewController: PagingMenuViewController, cellForItemAt index: Int) -> PagingMenuViewCell {
        let cell = viewController.dequeueReusableCell(withReuseIdentifier: "identifier", for: index)  as! OverlayMenuCell
        cell.titleLabel.text = dataSource[index].menu
        
        cell.setFrame(viewController.menuView, maskFrame: menuViewController.focusView.frame, animated: false)
        return cell
    }
    
    
    func menuViewController(viewController: PagingMenuViewController, widthForItemAt index: Int) -> CGFloat {
        OverlayMenuCell.sizingCell.titleLabel.text = dataSource[index].menu
        var referenceSize = UIView.layoutFittingCompressedSize
        referenceSize.height = viewController.view.bounds.height
        let size = OverlayMenuCell.sizingCell.systemLayoutSizeFitting(referenceSize, withHorizontalFittingPriority: UILayoutPriority.defaultLow, verticalFittingPriority: UILayoutPriority.defaultHigh)
        return size.width
    }
    
    func numberOfItemsForMenuViewController(viewController: PagingMenuViewController) -> Int {
        return dataSource.count
    }
}


extension ColumnMenuViewController: PagingContentViewControllerDataSource {
    func numberOfItemsForContentViewController(viewController: PagingContentViewController) -> Int {
        return dataSource.count
    }
    
    func contentViewController(viewController: PagingContentViewController, viewControllerAt index: Int) -> UIViewController {
        return dataSource[index].content
    }
}

extension ColumnMenuViewController: PagingMenuViewControllerDelegate {
    func menuViewController(viewController: PagingMenuViewController, didSelect page: Int, previousPage: Int) {
        contentViewController?.scroll(to: page, animated: true)
    }
    
    func menuViewController(viewController: PagingMenuViewController, willAnimateFocusViewTo index: Int, with coordinator: PagingMenuFocusViewAnimationCoordinator) {
        viewController.visibleCells.compactMap { $0 as? OverlayMenuCell }.forEach { cell in
            cell.setFrame(viewController.menuView, maskFrame: coordinator.beginFrame, animated: true)
        }
        
        coordinator.animateFocusView(alongside: { coordinator in
            viewController.visibleCells.compactMap { $0 as? OverlayMenuCell }.forEach { cell in
                cell.setFrame(viewController.menuView, maskFrame: coordinator.endFrame, animated: true)
            }
        }, completion: nil)
    }
    
    func menuViewController(viewController: PagingMenuViewController, willDisplay cell: PagingMenuViewCell, forItemAt index: Int) {
        (cell as? OverlayMenuCell)?.setFrame(viewController.menuView, maskFrame: menuViewController.focusView.frame, animated: false)
    }
}

extension ColumnMenuViewController: PagingContentViewControllerDelegate {
    func contentViewController(viewController: PagingContentViewController, didManualScrollOn index: Int, percent: CGFloat) {
        menuViewController.scroll(index: index, percent: percent, animated: false)
        menuViewController.visibleCells.forEach {
            let cell = $0 as! OverlayMenuCell
            cell.setFrame(menuViewController.menuView, maskFrame: menuViewController.focusView.frame, animated: false)
        }
    }
}
