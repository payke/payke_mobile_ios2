//
//  Copyright © 2018 Shin Yamamoto. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import FittedSheets

class MapViewController: UIViewController, MKMapViewDelegate, UISearchBarDelegate, CLLocationManagerDelegate {
    
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var itemView: UIView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var dictLabel: UILabel!
    
    
    var itemsArray:[StoreModel]!
    var ActivityIndicator: UIActivityIndicatorView!
    var pinArray: [MKAnnotation] = []
    var myLocationManager:CLLocationManager!
    var userLocation: CLLocationCoordinate2D!
    var itemId = ""
    var shops:[ShopLocation] = []
    
    
    @objc func goToItemPage(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.setData = (self.itemId,1)
        
        //performSegue(withIdentifier: "segue", sender: nil)
        let controller = SheetViewController(controller: UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "PSHTMLInTableViewController"), sizes: [.fullScreen])
        
        self.present(controller, animated: false, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.goToItemPage))
        self.imageView.addGestureRecognizer(tapGesture)
        self.imageView.isUserInteractionEnabled = true
        
        // Do any additional setup after loading the view, typically from a nib.
        // Initialize FloatingPanelController
        
        // ActivityIndicatorを作成＆中央に配置
        ActivityIndicator = UIActivityIndicatorView()
        ActivityIndicator.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        ActivityIndicator.center = self.view.center
        
        // クルクルをストップした時に非表示する
        ActivityIndicator.hidesWhenStopped = true
        ActivityIndicator.startAnimating()
        // 色を設定
        ActivityIndicator.style = UIActivityIndicatorView.Style.gray
        
        //Viewに追加
        self.view.addSubview(ActivityIndicator)
        
        let status = CLLocationManager.authorizationStatus()
        if status == CLAuthorizationStatus.restricted || status == CLAuthorizationStatus.denied {
            return
        }
        
        myLocationManager = CLLocationManager()
        myLocationManager.delegate = self
        if status == CLAuthorizationStatus.notDetermined {
            myLocationManager.requestWhenInUseAuthorization()
        }
        
        if !CLLocationManager.locationServicesEnabled() {
            return
        }
        
        myLocationManager.desiredAccuracy = kCLLocationAccuracyBest
        myLocationManager.distanceFilter = kCLDistanceFilterNone
        
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            myLocationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = manager.location {
            print("緯度：\(location.coordinate.latitude)")
            print("経度：\(location.coordinate.longitude)")
            
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude,
                                                longitude: location.coordinate.longitude)
            let span = MKCoordinateSpan(latitudeDelta: 0.4425100023575723,
                                        longitudeDelta: 0.28543697435880233)
            let region = MKCoordinateRegion(center: center, span: span)
            
            userLocation = CLLocationCoordinate2DMake(manager.location!.coordinate.latitude, manager.location!.coordinate.longitude)
            
            mapView.region = region
            mapView.showsCompass = true
            mapView.showsUserLocation = true
            mapView.delegate = self
            
            
            
            self.reloadData(la: location.coordinate.latitude, lo: location.coordinate.longitude)
        }
    }
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.tintColor = #colorLiteral(red: 0, green: 0.686277926, blue: 0.9266666174, alpha: 1)
    }
    
    
    
    
    
    func reloadData(la:CLLocationDegrees,lo:CLLocationDegrees){
        
        
        
        var url = "\(Api.url.apiUrl):\(Api.url.apiPort)/v2/customer/item/like_list?"
            + "&auth=\(Api.url.auth)"
            + "&user_id=\(Api.url.userId)"
            + "&lang=2"
        
        var getJson: NSDictionary!
        
        if let url = URL(string: url) {
            
            
            let req = NSMutableURLRequest(url: url)
            req.httpMethod = "GET"
            // req.httpBody = "userId=\(self.userId)&code=\(self.code)".data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: req as URLRequest, completionHandler: { (data, resp, err) in
                //                    print(resp!.url!)
                //                    print(NSString(data: data!, encoding: Str ing.Encoding.utf8.rawValue) as Any)
                
                // 受け取ったdataをJSONパース、エラーな らcatchへジャンプ
                
                do {
                    
                    
                    getJson = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    
                    var arr:Array = ((getJson["items"] as! NSArray) as Array)
                    
                    let myGroup = DispatchGroup()
                    
                    for y in arr{
                        
                        myGroup.enter()
                        
                        
                        
                        let id = self.getJsonStr(ob: y, name: "id")
                        self.reloadGeo(productId: id, lat: la, lng: lo, imgUrl: self.getJsonStr(ob: y, name: "image"), ItemName: self.getJsonStr(ob: y, name: "name"), ItemId: self.getJsonStr(ob: y, name: "id"))
                        myGroup.leave()
                    }
                    
                    myGroup.notify(queue: .main) {
                        
                    }
                    
                } catch {
                    print ("json error")
                    
                    return
                }
            })
            task.resume()
        }
    }
    
    
    
    
    func reloadGeo(productId: String, lat: Double, lng: Double,imgUrl: String,ItemName: String,ItemId: String){
        
        var url = "\(Api.url.apiUrl):\(Api.url.apiPort)/v2/customer/map/jan_and_latlng_nearer?"
            + "&auth=\(Api.url.auth)"
            + "&user_id=\(Api.url.userId)"
            + "&lang=2"
            + "&item_id=\(productId)"
            + "&lat=\(lat)"
            + "&lng=\(lng)"
        
        var getJson: NSDictionary!
        
        if let url = URL(string: url) {
            
            print ("☔️2 \(url)")
            
            let req = NSMutableURLRequest(url: url)
            req.httpMethod = "GET"
            // req.httpBody = "userId=\(self.userId)&code=\(self.code)".data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: req as URLRequest, completionHandler: { (data, resp, err) in
                //                    print(resp!.url!)
                //                    print(NSString(data: data!, encoding: Str ing.Encoding.utf8.rawValue) as Any)
                
                // 受け取ったdataをJSONパース、エラーな らcatchへジャンプ
                
                do {
                    
                    getJson = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    
                    var arr:Array = ((getJson["store"] as! NSArray) as Array)
                    
                    let myGroup = DispatchGroup()
                    
                    for y in arr{
                        
                        myGroup.enter()
                        
                        var dist: Double = Double(self.getJsonStr(ob: y, name: "distance")) ?? 0.0
                        
                        
                        
                        
                        
                        let cl = CLLocationCoordinate2D(latitude: Double(self.getJsonStr(ob: y, name: "lat")) as! CLLocationDegrees, longitude: Double(self.getJsonStr(ob: y, name: "lng")) as! CLLocationDegrees)
                        
                        self.shops.append(ShopLocation(lat: Double(self.getJsonStr(ob: y, name: "lat")) as! Double,
                                                       long: Double(self.getJsonStr(ob: y, name: "lng")) as! Double,
                                                       addres: self.getJsonStr(ob: y, name: "address"),
                                                       restaurantName: self.getJsonStr(ob: y, name: "name"),
                                                       dict: "\(Int(dist * 1000 * 1.6)) km",
                            imgUrl: imgUrl, itemName: ItemName, itemId: ItemId))
                        
                        myGroup.leave()
                    }
                    
                    myGroup.notify(queue: .main) {
                        // self.setupMapView(la: la, lo: lo)
                        self.mapView.showAnnotations(self.shops, animated: true)
                        self.ActivityIndicator.stopAnimating()
                    }
                    
                } catch {
                    print ("json error")
                    
                    return
                }
            })
            task.resume()
        }
    }
    
    
    
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        
        
        
        if view.annotation! is MKUserLocation {
            print("UserTapped")
        }else{
            if let index = self.shops.index(of: view.annotation! as! ShopLocation) {
                
                self.itemId = self.shops[index].itemId
                self.itemView.isHidden = false
                self.itemNameLabel.text! = self.shops[index].itemName
                self.restaurantNameLabel.text! = self.shops[index].restaurantName
                self.addressLabel.text! = self.shops[index].addres
                self.dictLabel.text! = "現在地から \(self.shops[index].dict)"
                Api.Img.configureCell(with: self.shops[index].imgUrl, imageView: self.imageView)
                let overlays = mapView.overlays
                mapView.removeOverlays(overlays)
                getRoute(destLocation: CLLocationCoordinate2DMake(self.shops[index].lat, self.shops[index].lng))
            }
        }
        
        
        
        // do what you need with the index
    }
    
    func getRoute(destLocation:CLLocationCoordinate2D)
    {
        
        
        let sourcePlaceMark = MKPlacemark(coordinate: userLocation)
        let destinationPlaceMark = MKPlacemark(coordinate: destLocation)
        let directionRequest = MKDirections.Request()
        directionRequest.source = MKMapItem(placemark: sourcePlaceMark)
        directionRequest.destination = MKMapItem(placemark: destinationPlaceMark)
        directionRequest.transportType = .automobile
        
        let directions = MKDirections(request: directionRequest)
        directions.calculate { (response, error) in
            guard let directionResonse = response else {
                if let error = error {
                    print("we have error getting directions==\(error.localizedDescription)")
                }
                return
            }
            
            let route = directionResonse.routes[0]
            self.mapView.addOverlay(route.polyline, level: .aboveRoads)
            //　縮尺を設定
            let rect = route.polyline.boundingMapRect
            
            self.mapView.setRegion(MKCoordinateRegion(MKMapRect(x: rect.origin.x, y: rect.origin.y, width: rect.width * 1.3, height: rect.height * 1.3)), animated: true)
        }
        
    }
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = UIColor.blue
        renderer.lineWidth = 4.0
        return renderer
    }
    
    func getJsonImg(ob:AnyObject, name:String) -> String{
        
        if let title = ob["\(name)"] as? [String: Any]
        {
            if let url:String = title["url"] as! String{
                return url
            }else{
                return ""
            }
        }
        else
        {
            return ""
        }
    }
    
    func getJsonStr(ob:AnyObject, name:String) -> String{
        
        
        if let title = ob["\(name)"] as? String
        {
            return title
        }
        else
        {
            return ""
        }
    }
    
    
    
    
    
}

class ShopLocation: NSObject, MKAnnotation{
    
    var lat: Double
    var lng: Double
    var addres: String
    var restaurantName: String
    var itemName: String
    var itemId: String
    var dict: String
    var imgUrl: String
    var coordinate: CLLocationCoordinate2D
    
    init(lat:Double,long:Double,addres:String,restaurantName:String,dict:String,imgUrl:String,itemName:String,itemId:String){
        self.lat = lat
        self.lng = long
        self.coordinate = CLLocationCoordinate2DMake(lat, long)
        self.addres = addres
        self.restaurantName = restaurantName
        self.dict = dict
        self.imgUrl = imgUrl
        self.itemName = itemName
        self.itemId = itemId
    }
    
}

