

//
//  OverlayMapViewController.swift
//  iOS Sample
//
//  Copyright (c) 2017 Kazuhiro Hayashi
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

import UIKit
import PagingKit

class OverlayMapViewController: UIViewController {
    // initializes on code
    let contentViewController = PagingContentViewController()
    let menuViewController = PagingMenuViewController()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.tintColor = #colorLiteral(red: 0, green: 0.686277926, blue: 0.9266666174, alpha: 1)
    }
    
    
    let dataSource: [(menu: String, content: UIViewController)] = ["Map", "Timeline", "Coupon"].map {
        let title = $0
        switch $0{
        case "Map":
            let story = UIStoryboard(name: "Map", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController
            return (menu: title, content: vc)
       
        case "Coupon":
            let story = UIStoryboard(name: "Map", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "CouponViewController") as! CouponViewController
            return (menu: title, content: vc)
            
            
        default:
            let story = UIStoryboard(name: "Map", bundle: nil)
            let vc = story.instantiateViewController(withIdentifier: "HomeViewControllerDetail") as! HomeViewController
            return (menu: title, content: vc)
        }
    }
    
    
    
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        do {
            // needs to set this because it initialize menu vc on code.
            menuViewController.view.translatesAutoresizingMaskIntoConstraints = false
            
            // add content vc on this vc.
            addChild(menuViewController)
            view.addSubview(menuViewController.view)
            menuViewController.didMove(toParent: self)
            
            // set autolayout
            if #available(iOS 11.0, *) {
                view.addConstraints([
                    view.safeAreaLayoutGuide.topAnchor.constraint(equalTo: menuViewController.view.topAnchor),
                    view.safeAreaLayoutGuide.leftAnchor.constraint(equalTo: menuViewController.view.leftAnchor),
                    view.safeAreaLayoutGuide.rightAnchor.constraint(equalTo: menuViewController.view.rightAnchor)
                    ])
                
                menuViewController.view
                    .heightAnchor
                    .constraint(equalToConstant: 44)
                    .isActive = true
            } else {
                fatalError("works only ios 11.0~")
            }
            
            // set delegate and datasource
            menuViewController.delegate = self
            menuViewController.dataSource = self
            
        }
        
        do {
            // needs to set this because it initialize content vc on code.
            contentViewController.view.translatesAutoresizingMaskIntoConstraints = false
            
            // add content vc on this vc.
            addChild(contentViewController)
            view.addSubview(contentViewController.view)
            contentViewController.didMove(toParent: self)
            
            // set autolayout
            if #available(iOS 11.0, *) {
                view.addConstraints([
                    view.safeAreaLayoutGuide.leftAnchor.constraint(equalTo: contentViewController.view.leftAnchor),
                    view.safeAreaLayoutGuide.rightAnchor.constraint(equalTo: contentViewController.view.rightAnchor),
                    view.safeAreaLayoutGuide.bottomAnchor.constraint(equalTo: contentViewController.view.bottomAnchor),
                    ])
                
                
                contentViewController.view
                    .topAnchor
                    .constraint(equalTo: menuViewController.view.bottomAnchor)
                    .isActive = true
            } else {
                fatalError("works only ios 11.0~")
            }
            
            // set delegate and datasource
            contentViewController.delegate = self
            contentViewController.dataSource = self
        }
        
        menuViewController.register(nib: UINib(nibName: "OverlayMenuCell", bundle: nil), forCellWithReuseIdentifier: "identifier")
        menuViewController.registerFocusView(nib: UINib(nibName: "OverlayFocusView", bundle: nil), isBehindCell: true)
        menuViewController.reloadData(with: 0, completionHandler: { [weak self, menuViewController = menuViewController] (vc) in
            let cell = self?.menuViewController.currentFocusedCell as! OverlayMenuCell
            cell.setFrame(menuViewController.menuView, maskFrame: menuViewController.focusView.frame, animated: false)
        })
        contentViewController.scrollView.bounces = true
        contentViewController.reloadData(with: 0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension OverlayMapViewController: PagingMenuViewControllerDataSource {
    func menuViewController(viewController: PagingMenuViewController, cellForItemAt index: Int) -> PagingMenuViewCell {
        let cell = viewController.dequeueReusableCell(withReuseIdentifier: "identifier", for: index)  as! OverlayMenuCell
        cell.titleLabel.text = dataSource[index].menu
        
        cell.setFrame(viewController.menuView, maskFrame: menuViewController.focusView.frame, animated: false)
        return cell
    }
    
    func menuViewController(viewController: PagingMenuViewController, widthForItemAt index: Int) -> CGFloat {
        return viewController.view.bounds.width / CGFloat(dataSource.count)
    }
    
    
    var insets: UIEdgeInsets {
        if #available(iOS 11.0, *) {
            return view.safeAreaInsets
        } else {
            return .zero
        }
    }
    
    func numberOfItemsForMenuViewController(viewController: PagingMenuViewController) -> Int {
        return dataSource.count
    }
}


extension OverlayMapViewController: PagingContentViewControllerDataSource {
    func numberOfItemsForContentViewController(viewController: PagingContentViewController) -> Int {
        return dataSource.count
    }
    
    func contentViewController(viewController: PagingContentViewController, viewControllerAt index: Int) -> UIViewController {
        return dataSource[index].content
    }
}

extension OverlayMapViewController: PagingMenuViewControllerDelegate {
    func menuViewController(viewController: PagingMenuViewController, didSelect page: Int, previousPage: Int) {
        contentViewController.scroll(to: page, animated: true)
    }
    
    func menuViewController(viewController: PagingMenuViewController, willAnimateFocusViewTo index: Int, with coordinator: PagingMenuFocusViewAnimationCoordinator) {
        viewController.visibleCells.compactMap { $0 as? OverlayMenuCell }.forEach { cell in
            cell.setFrame(viewController.menuView, maskFrame: coordinator.beginFrame, animated: true)
        }
        
        coordinator.animateFocusView(alongside: { coordinator in
            viewController.visibleCells.compactMap { $0 as? OverlayMenuCell }.forEach { cell in
                cell.setFrame(viewController.menuView, maskFrame: coordinator.endFrame, animated: true)
            }
        }, completion: nil)
    }
    
    func menuViewController(viewController: PagingMenuViewController, willDisplay cell: PagingMenuViewCell, forItemAt index: Int) {
        (cell as? OverlayMenuCell)?.setFrame(viewController.menuView, maskFrame: menuViewController.focusView.frame, animated: false)
    }
}

extension OverlayMapViewController: PagingContentViewControllerDelegate {
    func contentViewController(viewController: PagingContentViewController, didManualScrollOn index: Int, percent: CGFloat) {
        menuViewController.scroll(index: index, percent: percent, animated: false)
        menuViewController.visibleCells.forEach {
            let cell = $0 as! OverlayMenuCell
            cell.setFrame(menuViewController.menuView, maskFrame: menuViewController.focusView.frame, animated: false)
        }
    }
}
