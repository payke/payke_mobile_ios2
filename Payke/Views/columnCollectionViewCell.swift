//
//  columnCollectionViewCell.swift
//  Payke
//
//  Created by ShinjiYamamoto on 2019/05/13.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//

import UIKit

protocol columnCollectionViewCellDelegate: class {
    
    func goToDetail2(data:ColumnModel)
}

class columnCollectionViewCell: UICollectionViewCell {
    
    weak var delegate: columnCollectionViewCellDelegate?
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var subNameLabel: UILabel!
        @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var mainNameLabel: UILabel!
    
    var item: ColumnModel? {
        didSet {
            self.updateView()
        }
    }
    
    @objc func goToDetail() {
        self.delegate?.goToDetail2(data: item!)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        self.productImageView.layer.cornerRadius = self.productImageView.frame.height * 0.1
        self.productImageView.clipsToBounds = true
        

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.goToDetail))
        self.productImageView.addGestureRecognizer(tapGesture)
        self.productImageView.isUserInteractionEnabled = true
        
        subNameLabel.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.8377300942)
        subNameLabel.layer.shadowOffset = CGSize(width: 0.3, height: 0.3)
        subNameLabel.layer.shadowOpacity = 1
        subNameLabel.layer.shadowRadius = 6
        
        
    }
    
    func updateView() {
        
        self.mainNameLabel.text! = self.item!.title!
        
        Api.Img.configureCell(with: item!.url!, imageView: self.productImageView)
    }

}


