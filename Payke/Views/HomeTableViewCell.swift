//
//  HomeTableViewCell.swift
//  Payke
//
//  Created by ShinjiYamamoto on 2019/04/14.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//

import UIKit

protocol HomeTableViewCellDelegate: class {
    func goToDetail(item: ItemModel)
    func goToZoom(itemview: UIImageView,item: ItemModel)
}

class  HomeTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var itemView: UIView!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemTitleLabel: UILabel!
    @IBOutlet weak var itemSubTitleLabel: UILabel!
    @IBOutlet weak var starImageView: UIImageView!
    @IBOutlet weak var starLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    
    @IBOutlet weak var itemBackView: UIView!
    
    @IBOutlet weak var commentLabel: UILabel!
    
    @IBOutlet weak var labelTopHeight: NSLayoutConstraint!
    @IBOutlet weak var labelBottomHeight: NSLayoutConstraint!
    
    @IBOutlet weak var viewTopHeight: NSLayoutConstraint!
    
    @IBOutlet weak var bottomCommentLabel: UILabel!
    
    var disabledHighlightedAnimation = false
    weak var delegate: RankingTableViewCellDelegate?
    
    var item: TimelineModel? {
        didSet {
            self.updateView()
        }
    }
    
    @objc func goToDetail(){
        //    self.delegate?.goToDetail(item: item!)
        self.delegate?.goToZoom(itemview: self.itemImageView, no: item!.item_id)
    }
    
    func updateView() {
        
        self.bottomCommentLabel.text! = self.item!.review_text
        
        if self.item?.item_type == "ITEM_COMMENT"{
            self.labelTopHeight.constant = 8
            self.labelBottomHeight.constant = 8
            self.viewTopHeight.constant = 16
            
            self.commentLabel.text! = self.item!.review_text
            
        }else if self.item?.item_type == "PAYKESHARE_ITEM_COMMENT"{
            self.labelTopHeight.constant = 8
            self.labelBottomHeight.constant = 8
            self.viewTopHeight.constant = 16
            self.commentLabel.text! = self.item!.comment_message
            
        }else if self.item?.item_type == "COLUMN_COMMENTS"{
            self.labelTopHeight.constant = 8
            self.viewTopHeight.constant = 16
            self.labelBottomHeight.constant = 8
            self.commentLabel.text! = self.item!.post_content
       
        }else{
            self.labelTopHeight.constant = 0
            self.labelBottomHeight.constant = 0
            self.viewTopHeight.constant = 8
            self.commentLabel.text! = ""
        }
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.goToDetail))
        self.itemImageView.addGestureRecognizer(tapGesture)
        self.itemImageView.isUserInteractionEnabled = true
        
        
       Api.Img.configureCell(with: item!.item_image, imageView: self.itemImageView)
        
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        itemView.layer.cornerRadius = 16
        itemView.layer.masksToBounds = true
        
        self.applyShadow(view: self.itemView)
        
    }
    
    func applyShadow(view: UIView){
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 3, height: 6)
        view.layer.shadowOpacity = 0.4
        view.layer.shadowRadius = 5.0
        view.clipsToBounds = false
        view.layer.borderColor = UIColor.darkGray.cgColor
        view.layer.borderWidth = 0.1
    }
    
    
    
}

