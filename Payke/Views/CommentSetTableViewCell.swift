//
//  CommentSetTableViewCell.swift
//  Payke
//
//  Created by ShinjiYamamoto on 2019/05/30.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//

import UIKit

protocol CommentSetTableViewCellDelegate: class {
    func starSelected(star: Int)
   
}

class CommentSetTableViewCell: UITableViewCell {

    
    @IBOutlet weak var cosmosView: CosmosView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var avg_cosmos: CosmosView!
    @IBOutlet weak var avg_cosmos_label: UILabel!
    
    var item: ItemModel? {
        didSet {
            self.updateView()
        }
    }
    
    func updateView(){
        
        if item?.rate == nil{
            self.avg_cosmos.rating = 0.0
            self.avg_cosmos_label.text! = "0"
        }else{
            self.avg_cosmos.rating = self.item!.rate
            self.avg_cosmos_label.text! = "\(self.item!.rate)"
        }
        
       
    }
    
    weak var delegate: CommentSetTableViewCellDelegate?
    
    override public func prepareForReuse() {
        // Ensures the reused cosmos view is as good as new
        cosmosView.prepareForReuse()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cosmosView.didFinishTouchingCosmos = { rating in
            
            print("💫 1")
            self.delegate?.starSelected(star: Int(rating))
        }
        cosmosView.rating = 0
        cosmosView.settings.starMargin = 5
         avg_cosmos.settings.updateOnTouch = false
        
        self.backView.layer.borderColor = UIColor.darkGray.cgColor
        self.backView.layer.borderWidth = 1
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
