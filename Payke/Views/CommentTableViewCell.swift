//
//  CommentTableViewCell.swift
//  Payke
//
//  Created by ShinjiYamamoto on 2019/05/21.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//

import UIKit

protocol CommentTableViewCellDelegate: class {
    func tappedReport(id: String)
    
}

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var commentLabel: UILabel!
    
    @IBOutlet weak var commentView: UIView!

    @IBOutlet weak var profileImageView: UIImageView!
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var starImageView: UIImageView!
    
    weak var delegate: CommentTableViewCellDelegate?
    
    var commentItem: CommentModel? {
        didSet {
            self.updateView()
        }
    }
    @IBAction func reportButtonTapped(_ sender: Any) {
        self.delegate?.tappedReport(id: self.commentItem!.item_review_id!)
    }
    
    func updateView(){
    
        switch self.commentItem?.star! {
        case "1":
            self.starImageView.image = UIImage(named: "star1")
        case "2":
            self.starImageView.image = UIImage(named: "star2")
        case "3":
            self.starImageView.image = UIImage(named: "star3")
        case "4":
            self.starImageView.image = UIImage(named: "star4")
        case "5":
            self.starImageView.image = UIImage(named: "star5")
        default:
            print("")
        }
        
        
        
        if profileImageView.image == nil {
            let image1 = UIImage(named: "avatar-male")
            profileImageView.image = image1
        }else{
            if let url:String = commentItem!.image_profile!{

                Api.Img.configureCell(with: url, imageView: self.profileImageView)
            }else{
                self.profileImageView.image! = UIImage(named: "avatar-male")!
            }
        }
        

        
        
        if let name:String = commentItem!.username!{
            
            if name == ""{
                self.userNameLabel.text! = "not Set"
            }else{
                self.userNameLabel.text! = name
            }
            
        }else{
            self.userNameLabel.text! = "not Set"
        }
        
        self.timeLabel.text! = self.commentItem!.review_date!
        
        
        
        
        self.commentLabel.text! = self.commentItem!.review!
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        
        self.commentView.layer.borderColor = UIColor.darkGray.cgColor
        self.commentView.layer.borderWidth = 1
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
