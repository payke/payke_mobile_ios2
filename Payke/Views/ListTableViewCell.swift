//
//  ListTableViewCell.swift
//  Payke
//
//  Created by ShinjiYamamoto on 2019/06/27.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//


import UIKit

protocol ListTableViewCellDelegate: class {
    
    func goToZoom(itemview: UIImageView,no: String)
}

class ListTableViewCell: UITableViewCell {
    
    @IBOutlet weak var noView: UIView!
    @IBOutlet weak var noLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemTitleLabel: UILabel!
    @IBOutlet weak var itemSubTitleLabel: UILabel!
    
    @IBOutlet weak var rankView: CosmosView!
    @IBOutlet weak var starLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    
    
    
    
    weak var delegate: ListTableViewCellDelegate?
    
    var item: ListModel? {
        didSet {
            self.updateView()
        }
    }
    
    @objc func goToDetail(){
        self.delegate?.goToZoom(itemview: self.itemImageView, no: item!.id)
    }
    
    func updateView() {
        if item?.star_avg == nil || item!.star_avg == 0.00001{
            self.rankView.rating = 0.0
            self.starLabel.text! = "not set"
        }else{
            self.rankView.rating = self.item!.star_avg
            self.starLabel.text! = "\(self.item!.star_avg)"
        }
        
        
        rankView.settings.updateOnTouch = false
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.goToDetail))
        self.itemImageView.addGestureRecognizer(tapGesture)
        self.itemImageView.isUserInteractionEnabled = true
        
        Api.Img.configureCell(with: item!.image, imageView: self.itemImageView)
        self.itemTitleLabel.text! = item!.title
        self.itemSubTitleLabel.text! = item!.name
        
        //        switch Double(Int(item!.rank)!) {
        //        case 0.0..<1.0:
        //            self.starImageView.image! = UIImage(named: "star1")!
        //        case 1.0..<1.5:
        //            self.starImageView.image! = UIImage(named: "star1.5")!
        //        case 1.5..<2.0:
        //            self.starImageView.image! = UIImage(named: "star2")!
        //        case 2.0..<2.5:
        //            self.starImageView.image! = UIImage(named: "star2.5")!
        //        case 2.5..<3.0:
        //            self.starImageView.image! = UIImage(named: "star3")!
        //        case 3.0..<3.5:
        //            self.starImageView.image! = UIImage(named: "star3.5")!
        //        case 3.5..<4.0:
        //            self.starImageView.image! = UIImage(named: "star4")!
        //        case 4.0..<4.5:
        //            self.starImageView.image! = UIImage(named: "star4.5")!
        //        case 4.5..<5.0:
        //            self.starImageView.image! = UIImage(named: "star5")!
        //        default:
        //            self.starImageView.image! = UIImage(named: "star0.5")!
        //        }
        
        
        
        if item!.like_count == 0{
            self.likeCountLabel.text! = "not set"
        }else{
            self.likeCountLabel.text! = "\(item!.like_count)"
        }
        
        
        
        
    }
    
}
