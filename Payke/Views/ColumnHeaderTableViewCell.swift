//
//  ColumnHeaderTableViewCell.swift
//  Payke
//
//  Created by ShinjiYamamoto on 2019/05/27.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//

import UIKit

class ColumnHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var mainTitleLabel: UILabel!
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLabel: UIButton!
    
    @IBOutlet weak var timeLabel: UILabel!
    
    
    var item: ColumnModel? {
        didSet {
            self.updateView()
        }
    }
    
    func updateView(){
        Api.Img.configureCell(with: self.item!.url!, imageView: self.headerImageView)
        self.mainTitleLabel.text! = (self.item?.title!)!
        
        self.timeLabel.text! = getDateFromTimeStamp(timeStamp: Double(self.item!.date_unix!))
        
        self.nameLabel.setTitle(self.item?.author, for: .normal)
        
    }

    func getDateFromTimeStamp(timeStamp : Double) -> String {
        
        let date = NSDate(timeIntervalSince1970: timeStamp)
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM YY, hh:mm a"
        // UnComment below to get only time
        //  dayTimePeriodFormatter.dateFormat = "hh:mm a"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
