//
//  SearchItemTableViewCell.swift
//  Payke
//
//  Created by ShinjiYamamoto on 2019/04/15.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//

import UIKit


protocol SearchItemTableViewCellDelegate: class {
    
    func goToZoom(itemview: UIImageView,item: ItemModel)
}

class SearchItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemTitleLabel: UILabel!
    @IBOutlet weak var itemSubTitleLabel: UILabel!
    @IBOutlet weak var starImageView: UIImageView!
    @IBOutlet weak var starLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    
    var disabledHighlightedAnimation = false
    weak var delegate: SearchItemTableViewCellDelegate?
    
    var item: ItemModel? {
        didSet {
            self.updateView()
        }
    }
    
    @objc func goToDetail(){
        self.delegate?.goToZoom(itemview: self.itemImageView, item: item!)
    }
    
    func updateView() {
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.goToDetail))
        self.itemImageView.addGestureRecognizer(tapGesture)
        self.itemImageView.isUserInteractionEnabled = true
        
        
        Api.Img.configureCell(with: item!.imgUrl[0], imageView: self.itemImageView)
        self.itemTitleLabel.text! = item!.title
        self.itemSubTitleLabel.text! = item!.subTitle
        
        switch item!.rate {
        case 0.0..<1.0:
            self.starImageView.image! = UIImage(named: "star1")!
        case 1.0..<1.5:
            self.starImageView.image! = UIImage(named: "star1.5")!
        case 1.5..<2.0:
            self.starImageView.image! = UIImage(named: "star2")!
        case 2.0..<2.5:
            self.starImageView.image! = UIImage(named: "star2.5")!
        case 2.5..<3.0:
            self.starImageView.image! = UIImage(named: "star3")!
        case 3.0..<3.5:
            self.starImageView.image! = UIImage(named: "star3.5")!
        case 3.5..<4.0:
            self.starImageView.image! = UIImage(named: "star4")!
        case 4.0..<4.5:
            self.starImageView.image! = UIImage(named: "star4.5")!
        case 4.5..<5.0:
            self.starImageView.image! = UIImage(named: "star5")!
        default:
            self.starImageView.image! = UIImage(named: "star0.5")!
        }
        
        self.starLabel.text! = "\(item!.rate)"
        self.likeCountLabel.text! = "\(item!.likeCount)"
        
    }
    
    
    
}
