//
//  columnCollectionReusableView.swift
//  Payke
//
//  Created by ShinjiYamamoto on 2019/05/13.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//

import UIKit

protocol columnCollectionReusableViewDelegate: class {
    
    func goToDetail(data:ColumnModel)
}

class columnCollectionReusableView: UICollectionReusableView {
    
    @IBOutlet weak var headerImage: UIImageView!
    
    @IBOutlet weak var mainTitleLabel: UILabel!
    
    @IBOutlet weak var profileNameLabel: UILabel!
    
    
    weak var delegate: columnCollectionReusableViewDelegate?
    
    var item: ColumnModel? {
        didSet {
            self.updateView()
        }
    }
    
    @objc func goToDetail() {
        self.delegate?.goToDetail(data: item!)
    }
    
    func updateView() {
        
        self.profileNameLabel.text! = (self.item?.author!)!
        
        self.mainTitleLabel.text! = self.item!.title!
        
        Api.Img.configureCell(with: item!.url!, imageView: self.headerImage)
    }

    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.goToDetail))
        self.headerImage.addGestureRecognizer(tapGesture)
        self.headerImage.isUserInteractionEnabled = true
    
        
        mainTitleLabel.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.8377300942)
        mainTitleLabel.layer.shadowOffset = CGSize(width: 0.3, height: 0.3)
        mainTitleLabel.layer.shadowOpacity = 1
        mainTitleLabel.layer.shadowRadius = 6
        
        
        self.headerImage.layer.cornerRadius = self.headerImage.frame.height * 0.1
        self.headerImage.clipsToBounds = true
        
    }
    
    
}
