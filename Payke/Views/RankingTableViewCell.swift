//
//  RankingTableViewCell.swift
//  Payke
//
//  Created by ShinjiYamamoto on 2019/04/14.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//

import UIKit

protocol RankingTableViewCellDelegate: class {
   
    func goToZoom(itemview: UIImageView,no: String)
}

class RankingTableViewCell: UITableViewCell {
    
    @IBOutlet weak var noView: UIView!
    @IBOutlet weak var noLabel: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemTitleLabel: UILabel!
    @IBOutlet weak var itemSubTitleLabel: UILabel!
    @IBOutlet weak var rankView: CosmosView!
    @IBOutlet weak var starLabel: UILabel!
    @IBOutlet weak var likeCountLabel: UILabel!
    


    
    weak var delegate: RankingTableViewCellDelegate?
    
    var item: RankingModel? {
        didSet {
            self.updateView()
        }
    }
    
    @objc func goToDetail(){
        self.delegate?.goToZoom(itemview: self.itemImageView, no: item!.item_id)
    }
    
    func updateView() {
        
        if item?.star_avg == nil || item!.star_avg == ""{
            self.rankView.rating = 0.0
            self.starLabel.text! = "not set"
        }else{
            self.rankView.rating = Double(Int(self.item!.star_avg)!)
            self.starLabel.text! = "\(self.item!.star_avg)"
        }
        
        
        rankView.settings.updateOnTouch = false
   
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.goToDetail))
        self.itemImageView.addGestureRecognizer(tapGesture)
        self.itemImageView.isUserInteractionEnabled = true
        
        Api.Img.configureCell(with: item!.thumbnail_url, imageView: self.itemImageView)
        self.itemTitleLabel.text! = item!.title
        self.itemSubTitleLabel.text! = item!.name
        

        if item!.like_count == ""{
            self.likeCountLabel.text! = "not set"
        }else{
            self.likeCountLabel.text! = item!.like_count
        }
    }
}
