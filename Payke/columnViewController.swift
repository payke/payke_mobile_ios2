//
//  columnViewController.swift
//  Payke
//
//  Created by ShinjiYamamoto on 2019/05/13.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//

import UIKit
import FittedSheets

class columnViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {


    @IBOutlet weak var collectionView: UICollectionView!

     var items:[ColumnModel] = []
   
    var category = 0
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.dataSource = self
        self.collectionView.delegate = self
        
        switch self.category {
        case 0:
            accessURL()
        case 66:
            accessURL2()
        case 45:
            accessURL2()
        case 80:
            accessURL2()
        case 87:
            accessURL2()
        case 73:
            accessURL2()
        case 59:
            accessURL2()
        case 52:
            accessURL2()
            
        default:
            print("")
        }
        
        
    }
    
    func accessURL2(){
        
      print("😷1")
     
        
        var getJson: NSDictionary!
        

        
        // API接続先 ALL
        let urlStr = "https://column.payke.co.jp/wp-admin/admin-ajax.php?action=list&lang=ja&cat=\(self.category)"
        
        
        
        if let url = URL(string: urlStr) {
            
              print ("dwa💤 \(url)")
            
            let req = NSMutableURLRequest(url: url)
            req.httpMethod = "GET"
            // req.httpBody = "userId=\(self.userId)&code=\(self.code)".data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: req as URLRequest, completionHandler: { (data, resp, err) in
                //                    print(resp!.url!)
                //                    print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as Any)
                
                // 受け取ったdataをJSONパース、エラーな らcatchへジャンプ

                
                do {

                    getJson = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    

                    var arr:Array = ((getJson["data"] as! NSArray) as Array)

                    for i in arr{
                      
                        self.items.append(ColumnModel(id: self.getJsonInt(ob: i, name: "id"), title: self.getJsonStr(ob: i, name: "title"), author: self.getJsonStr(ob: i, name: "author"), trid: self.getJsonInt(ob: i, name: "trid"), ad: self.getJsonInt(ob: i, name: "ad"), like_count: self.getJsonStr(ob: i, name: "like_count"), date_unix: self.getJsonInt(ob: i, name: "date_unix"), slug: "", url: self.getImgStr(ob: i, name: "image"), width: "", height: ""))
                        
                        
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                        }
                    }
                    
                    
                } catch {
                    print ("json error")

                    return
                }
            })
            task.resume()
        }
        
        
        
    }
    
    func accessURL(){
        
        print("ddkkwdaajs1 ")
        
        var CastAnotherThemeList: [Any] = []
        
        var getJson: NSDictionary!
        
        // API接続先 ALL
        let urlStr = "https://column.payke.co.jp/wp-admin/admin-ajax.php?action=list&lang=ja"
        
        if let url = URL(string: urlStr) {
            
            
            
            let req = NSMutableURLRequest(url: url)
            req.httpMethod = "GET"
            // req.httpBody = "userId=\(self.userId)&code=\(self.code)".data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: req as URLRequest, completionHandler: { (data, resp, err) in
                //                    print(resp!.url!)
                //                    print(NSString(data: data!, encoding: String.Encoding.utf8.rawValue) as Any)
                
                // 受け取ったdataをJSONパース、エラーな らcatchへジャンプ
                do {
                    
                    
                    getJson = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    
                    
                    
                    
                    var arr:Array = ((getJson["data"] as! NSArray) as Array)

                    
                    for i in arr{
                        
                        self.items.append(ColumnModel(id: self.getJsonInt(ob: i, name: "id"), title: self.getJsonStr(ob: i, name: "title"), author: self.getJsonStr(ob: i, name: "author"), trid: self.getJsonInt(ob: i, name: "trid"), ad: self.getJsonInt(ob: i, name: "ad"), like_count: self.getJsonStr(ob: i, name: "like_count"), date_unix: self.getJsonInt(ob: i, name: "date_unix"), slug: "", url: self.getImgStr(ob: i, name: "image"), width: "", height: ""))
                        
                        
                        DispatchQueue.main.async {
                            self.collectionView.reloadData()
                        }
                    }
                    
                    
                } catch {
                    print ("json error")
                    return
                }
            })
            task.resume()
        }
        
        
        
    }
    
    func getImgStr(ob:AnyObject, name:String) -> String{
        
        var image = (ob["image"] as? NSDictionary)!
        var url = (image["url"] as? String)!
        return url
    }
    
    func getJsonStr(ob:AnyObject, name:String) -> String{
        
        var name = (ob["\(name)"] as? String)!
        if !(name is NSNull){
            return name
        }else{
            return ""
        }
    }
    
    func getJsonInt(ob:AnyObject, name:String) -> Int{
        var name = (ob["\(name)"] as? Int)!
        if !(name is NSNull){
            return name
        }else{
            return 999
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        return items.count - 1
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        

        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "columnCollectionViewCell", for: indexPath) as! columnCollectionViewCell
        cell.item =  self.items[indexPath.row + 1]
        cell.delegate = self
        
        return cell

    
    }



    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "columnCollectionReusableView", for: indexPath) as! columnCollectionReusableView

        header.delegate = self
        
        if self.items.count != 0{
             header.item =  self.items[0]
        }
        
        
        return header
        
    }

    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        
       return CGSize(width: collectionView.frame.size.width / 2 - 1, height: collectionView.frame.size.width / 2 - 1)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {

         return CGSize(width: self.view.bounds.width, height: 260)

    }
    
}


extension columnViewController: columnCollectionReusableViewDelegate {
    func goToDetail(data:ColumnModel) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
      
        appDelegate.columnItem = data
        
        let controller = SheetViewController(controller: UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "DetailColumnViewController"), sizes: [.fullScreen])
        
        self.present(controller, animated: false, completion: nil)
    }
    
 
}


extension columnViewController: columnCollectionViewCellDelegate {
    func goToDetail2(data:ColumnModel) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        appDelegate.columnItem = data
        
        let controller = SheetViewController(controller: UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "DetailColumnViewController"), sizes: [.fullScreen])
        
        self.present(controller, animated: false, completion: nil)
    }
    
    
}
