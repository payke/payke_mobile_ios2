//
//  JsonApi.swift
//  Payke
//
//  Created by ShinjiYamamoto on 2019/07/03.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//

import Foundation

class JsonApi {
    func getJsonStr(ob:AnyObject, name:String) -> String{
        var name = (ob["\(name)"] as? String)!
        if !(name is NSNull){
            return name
        }else{
            return ""
        }
    }
    
    func getJsonInt(ob:AnyObject, name:String) -> Int{
        var name = (ob["\(name)"] as? Int)!
        if !(name is NSNull){
            return name
        }else{
            return 999
        }
    }
}


