
import Foundation
import Kingfisher

class ImgApi {
    func configureCell(with URLString: String, imageView: UIImageView) {
        
        
        let processor = ResizingImageProcessor(referenceSize: CGSize(width: imageView.frame.width * 0.8, height: imageView.frame.height * 0.8), mode: .aspectFill)
        
        ImageCache.default.maxDiskCacheSize = 50 * 740 * 740
        imageView.kf.indicatorType = .activity
        
        imageView.kf.setImage(with: URL(string: URLString),
                              placeholder: nil,
                              options: [
                                .processor(processor),
                                .scaleFactor(UIScreen.main.scale),
                                .transition(.fade(0.6)),
                                .cacheOriginalImage
            ],
                              progressBlock: { (receivedSize, totalSize) -> () in
                                print("Download Progress: \(receivedSize)/\(totalSize)")
        },
                              completionHandler: { (image, error, cacheType, imageURL) -> () in
                                // ★１回表示した画像のみ実行される！
                                
                                if error == nil{
                                    
                                }else{
                                    print("Downloaded Error")
                                    
                                    if imageView.image == nil {
                                        let image1 = UIImage(named: "paykeLogo")
                                        imageView.image = image1
                                    }
                                }
                                
                                
                                print("Downloaded and set!")
        }
            
        )}
}




