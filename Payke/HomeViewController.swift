//
//  HomeViewController.swift
//  Payke
//
//  Created by ShinjiYamamoto on 2019/04/13.
//  Copyright © 2019 ShinjiYamamoto. All rights reserved.
//

import UIKit
import FittedSheets

class HomeViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var img:UIImage?
    
    var items:[TimelineModel] = []
    
    
    private var selectedImageView: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
       
        
        reloadData()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 60 // 初期値なんでもいい
        tableView.rowHeight = UITableView.automaticDimension // 高さ自動設定
        
    }
    
    
    func reloadData(){
        
         var url = "\(Api.url.apiUrl):\(Api.url.apiPort)/v2/customer/timeline?" + "&enablePaykeshare=1" + "&lang=1" + "&user_id=\(Api.url.userId)" + "&auth=\(Api.url.auth)"
        
         var getJson: NSDictionary!
        
        if let url = URL(string: url) {
            
            print ("☔️ \(url)")
            
            let req = NSMutableURLRequest(url: url)
            req.httpMethod = "GET"
            // req.httpBody = "userId=\(self.userId)&code=\(self.code)".data(using: String.Encoding.utf8)
            let task = URLSession.shared.dataTask(with: req as URLRequest, completionHandler: { (data, resp, err) in
                //                    print(resp!.url!)
                //                    print(NSString(data: data!, encoding: Str ing.Encoding.utf8.rawValue) as Any)
                
                // 受け取ったdataをJSONパース、エラーな らcatchへジャンプ
                
                print("😷2")
                
                do {
                    
                    print("😷3")
                    
                    
                    getJson = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                    
                    print ("☔️1 \(getJson!)")
                    
                    var arr:Array = ((getJson["items"] as! NSArray) as Array)
                    print ("arr💤 \(arr)")
                    let myGroup = DispatchGroup()
                    
                    for y in arr{
                        print ("ardwar💤 \(y)")
                        
                        myGroup.enter()
                        
                        let data = TimelineModel(age: self.getJsonStr(ob: y, name: "age"),
                                                 content_score: self.getJsonStr(ob: y, name: "content_score"),
                                                 created_at: self.getJsonStr(ob: y, name: "created_at"),
                                                 display_type: self.getJsonStr(ob: y, name: "display_type"),
                                                 id: self.getJsonStr(ob: y, name: "id"),
                                                 is_item_liked: self.getJsonStr(ob: y, name: "is_item_liked"),
                                                 item_desc_title: self.getJsonStr(ob: y, name: "item_desc_title"),
                                                 item_id: self.getJsonStr(ob: y, name: "item_id"),
                                                 item_image: self.getJsonStr(ob: y, name: "item_image"),
                                                 item_like_count: self.getJsonStr(ob: y, name: "item_like_count"),
                                                 item_name: self.getJsonStr(ob: y, name: "item_name"),
                                                 item_review_qty: self.getJsonStr(ob: y, name: "item_review_qty"),
                                                 item_star_avg: self.getJsonStr(ob: y, name: "item_star_avg"),
                                                 item_type: self.getJsonStr(ob: y, name: "item_type"),
                                                 review_text: self.getJsonStr(ob: y, name: "review_text"),
                                                 review_username: self.getJsonStr(ob: y, name: "review_username"),
                                                 scan_count: self.getJsonStr(ob: y, name: "scan_count"),
                                                 scan_user_image: self.getJsonStr(ob: y, name: "scan_user_image"),
                                                 scan_username: self.getJsonStr(ob: y, name: "scan_username"),
                                                 sex: self.getJsonStr(ob: y, name: "sex"),
                                                 user_id: self.getJsonStr(ob: y, name: "user_id"),
                                                 review_user_id: self.getJsonStr(ob: y, name: "review_user_id"),
                                                 review_user_image: self.getJsonStr(ob: y, name: "review_user_image"),
                                                 item_category_name: self.getJsonStr(ob: y, name: "item_category_name"),
                                                 like_user_image: self.getJsonStr(ob: y, name: "like_user_image"),
                                                 comment_like_count: self.getJsonStr(ob: y, name: "comment_like_count"),
                                                 shareitemlist_id: self.getJsonStr(ob: y, name: "shareitemlist_id"),
                                                 like_user_id: self.getJsonStr(ob: y, name: "like_user_id"),
                                                 like_username: self.getJsonStr(ob: y, name: "like_username"),
                                                 shareitem_like_count: self.getJsonStr(ob: y, name: "shareitem_like_count"),
                                                 shareitem_title: self.getJsonStr(ob: y, name: "shareitem_title"),
                                                 comment_user_id: self.getJsonStr(ob: y, name: "comment_user_id"),
                                                 comment_username: self.getJsonStr(ob: y, name: "comment_username"),
                                                 comment_user_image: self.getJsonStr(ob: y, name: "comment_user_image"),
                                                 comment_user_message: self.getJsonStr(ob: y, name: "comment_user_message"),
                                                 user_uploaded_filename: self.getJsonStr(ob: y, name: "user_uploaded_filename"),
                                                 original_item_id: self.getJsonStr(ob: y, name: "original_item_id"),
                                                 original_shareitem_filename: self.getJsonStr(ob: y, name: "original_shareitem_filename"),
                                                 comment_message: self.getJsonStr(ob: y, name: "comment_message"),
                                                 shareitem_concept: self.getJsonStr(ob: y, name: "shareitem_concept"),
                                                 image_1: self.getJsonStr(ob: y, name: "image_1"),
                                                 trid: self.getJsonStr(ob: y, name: "trid"),
                                                 element_id: self.getJsonStr(ob: y, name: "element_id"),
                                                 post_title: self.getJsonStr(ob: y, name: "post_title"),
                                                 post_like_count: self.getJsonStr(ob: y, name: "post_like_count"),
                                                 post_content: self.getJsonStr(ob: y, name: "post_content"),
                                                 review_id: self.getJsonStr(ob: y, name: "review_id"),
                                                 star_avg: self.getJsonStr(ob: y, name: "star_avg"),
                                                 is_comment_liked: self.getJsonStr(ob: y, name: "is_comment_liked"),
                                                 image: self.getJsonImg(ob: y, name: "image"))
                        
                        self.items.append(data)
                        myGroup.leave()
                    }
                    
                    myGroup.notify(queue: .main) {
                        self.tableView.reloadData()
                    }
                    
                    
                    
                    print ("💭💤")
                    
                    
                    
                    
                } catch {
                    print ("json error")
                    print("😷4")
                    return
                }
            })
            task.resume()
        }
    }

    func getJsonImg(ob:AnyObject, name:String) -> String{
        
        if let title = ob["\(name)"] as? [String: Any]
        {
            if let url:String = title["url"] as! String{
                return url
            }else{
                return ""
            }
        }
        else
        {
            return ""
        }
    }
    
    func getJsonStr(ob:AnyObject, name:String) -> String{
        
        
        if let title = ob["\(name)"] as? String
        {
            return title
        }
        else
        {
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch items[indexPath.row].display_type {
        case "LARGE_IMAGE":
            let cell = tableView.dequeueReusableCell(withIdentifier: "LargeImageTableViewCell", for: indexPath) as! LargeImageTableViewCell
            

            
            cell.item = items[indexPath.row]
            
            return cell
        case "MEDIUM_IMAGE":
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCell", for: indexPath) as! HomeTableViewCell
            
            cell.delegate = self
            
            cell.item = items[indexPath.row]
            
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "NoImageTableViewCell", for: indexPath) as! NoImageTableViewCell
            
      //      cell.delegate = self
            
            cell.item = items[indexPath.row]
            
            return cell
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            return .lightContent
        }
    }
    
    
}
extension HomeViewController: RankingTableViewCellDelegate {
    func goToDetail(no: String) {
        
    }
    
    func goToZoom(itemview: UIImageView, no: String) {
        
    }
    

}
